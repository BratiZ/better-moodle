export enum QuestionLevelColorEnum {
  LVL_0 = '',
  LVL_1 = '#59e23a',
  LVL_2 = '#ffd653',
  LVL_3 = '#ff5f5a'

}
