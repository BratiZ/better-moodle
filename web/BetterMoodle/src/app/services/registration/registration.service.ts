import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, mapTo, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {

  constructor(private http: HttpClient) { }

  register(user: User) : Observable<any> {
    return this.http.post(`api/register`, user);
  }
}

export interface User {
  login: string;
  email: string;
  password: string;
}
