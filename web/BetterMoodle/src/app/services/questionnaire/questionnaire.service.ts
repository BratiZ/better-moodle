import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {TranslateService} from '@ngx-translate/core';
import {QuestionnaireModel} from '../../models/questionnaire/questionnaire.model';
import {UserQuestionnaireModel} from '../../models/questionnaire/useranswers/user-questionnaire.model';
import {Observable} from 'rxjs';
import {UserPersonalityTypeModel} from '../../models/userdata/user.model';

@Injectable({providedIn: 'root'})
export class QuestionnaireService {

  constructor(
    private http: HttpClient,
    private translate: TranslateService,
  ) {
  }

  sendAnswersFromQuestionnaire(answers: UserQuestionnaireModel): Observable<UserPersonalityTypeModel> {
    const len = this.translate.getBrowserLang();
    const completeUrl = '/api/' + len + '/questionnaire/results';

    return this.http.post<UserPersonalityTypeModel>(
      completeUrl,
      answers
    );
  }

  getQuestionnaire(key: string): Promise<QuestionnaireModel> {
    return new Promise<QuestionnaireModel>((resolve, reject) => {
      const len = this.translate.getBrowserLang();
      const completeUrl = '/api/' + len + '/questionnaire/' + key;
      this.http.get(completeUrl)
        .toPromise()
        .then((result: QuestionnaireModel) => {
          resolve(result); // todo add cast to object
        })
        .catch(error => {
          console.error(`error getting data : ${completeUrl}`, error);
          reject();
        });
    });
  }

}
