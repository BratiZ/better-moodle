import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, mapTo, tap} from 'rxjs/operators';
import {HttpJwtResponseModel} from '../../models/auth/http-jwt-response.model';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  private readonly JWT_TOKEN = 'JWT_TOKEN'; // todo encode this!
  private readonly USER_ROLE = 'USER_ROLE'; // todo encode this!
  private readonly USER_TYPE = 'USER_TYPE'; // todo encode this!
  private readonly REFRESH_TOKEN = 'REFRESH_TOKEN'; // todo encode this!
  private loggedUser: string;

  constructor(
    private http: HttpClient,
  ) {
  }

  login(user: { login: string, password: string }): Observable<boolean> {
    return this.http.post('/api/login', user)
      .pipe(
        tap((tokenModel: HttpJwtResponseModel) => this.doLoginUser(user.login, tokenModel)),
        mapTo(true),
        catchError(error => {
          console.error(error.error);
          return of(false);
        }));
  }

  public logout(): Observable<any> { // todo add type
    return this.http.post<any>('/api/logout', {
      refreshToken: this.getRefreshToken() // todo refresh token
    })
      .pipe(
        tap(() => this.doLogoutUser()),
        mapTo(true),
        catchError(error => {
          alert(error.error);
          return of(false);
        }));
  }


  private doLoginUser(username: string, tokenModel: HttpJwtResponseModel): void {
    this.loggedUser = username;
    this.storeUserData(tokenModel);
    console.log(tokenModel.userType);
  }


  private storeUserData(tokenModel: HttpJwtResponseModel): void {
    this.storeJwtToken(tokenModel.jwt);
    this.storeJwtRefreshToken(tokenModel.refreshToken);
    this.storeUserRole(tokenModel.userRole);
    this.storeUserType(tokenModel.userType);
  }

  public doLogoutUser(): void {
    this.loggedUser = null;
    this.removeUserData();
  }

  private removeUserData(): void {
    localStorage.removeItem(this.JWT_TOKEN);
    localStorage.removeItem(this.REFRESH_TOKEN);
    localStorage.removeItem(this.getUserRole());
    localStorage.removeItem(this.getUserType());
  }

  public refreshToken(): Observable<any> { // todo add type
    return this.http.post<any>('/api/refresh', {
      refreshToken: this.getRefreshToken()
    })
      .pipe(
        tap((tokens: HttpJwtResponseModel) => {
          this.storeJwtToken(tokens.jwt);
        }));
  }


  public isLoggedIn(): boolean {
    return !!this.getJwtToken();
  }

  private getRefreshToken(): string {
    return localStorage.getItem(this.REFRESH_TOKEN);
  }

  private storeJwtToken(jwt: string): void {
    localStorage.setItem(this.JWT_TOKEN, jwt);
  }

  private storeJwtRefreshToken(refreshToken: string): void {
    localStorage.setItem(this.REFRESH_TOKEN, refreshToken);
  }

  private storeUserRole(userRole: string): void {
    localStorage.setItem(this.USER_ROLE, userRole);
  }

  private storeUserType(userType: string): void {
    localStorage.setItem(this.USER_TYPE, userType);
  }

  public getJwtToken(): string {
    return localStorage.getItem(this.JWT_TOKEN);
  }

  public getUserRole(): string {
    return localStorage.getItem(this.USER_ROLE);
  }

  public getUserType(): string {
    return localStorage.getItem(this.USER_TYPE);
  }

  public setUserType(newRole: string): void {
    return this.storeUserType(newRole);
  }
}


