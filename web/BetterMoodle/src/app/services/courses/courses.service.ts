import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {CourseCreateModel, CourseDataModel, CourseListModel, CourseModel, EditedCourseModel} from '../../models/courses/course.model';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {

  constructor(
    private http: HttpClient
  ) {
  }

  public getAvailableCourses(): Observable<CourseModel[]> {
    return this.http.get<CourseModel[]>('api/courses');
  }

  public getAllCourses(): Observable<CourseListModel[]> {
    return this.http.get<CourseListModel[]>('api/courses');
  }

  public getCourseInfo(courseId: string): Observable<CourseDataModel> {
    return this.http.get<CourseDataModel>(`api/courses/${courseId}/data`);
  }

  public stopCourse(courseId: string): Observable<string> {
    return this.http.put<string>(`api/courses/${courseId}/stop`, null);
  }

  public deleteCourse(courseId: string): Observable<string> {
    return this.http.put<string>(`api/courses/${courseId}/delete`, null);
  }

  public createNewCourse(courseModel: CourseCreateModel): Observable<string> {
    return this.http.post<string>(`api/courses`, courseModel);
  }

  public editCourse(courseId: string, courseModel: EditedCourseModel): Observable<CourseDataModel> {
    return this.http.put<CourseDataModel>(`api/courses/${courseId}`, courseModel);
  }

  public unarchiveCourse(courseId: string): Observable<string> {
    return this.http.put<string>(`api/courses/${courseId}/unarchive`, null);
  }

  public getCoursesForSubject(subjectId: string): Observable<CourseListModel[]> {
    return this.http.get<CourseListModel[]>(`api/subjects/${subjectId}/courses`);
  }
}
