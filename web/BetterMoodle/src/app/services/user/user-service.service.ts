import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {TeacherModel, UserDataModel} from '../../models/userdata/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {
  constructor(private http: HttpClient) {
  }

  public getAll(): Observable<UserDataModel[]> {
    return this.http.get<UserDataModel[]>('/api/users/users');
  }

  public register(user) {
    return this.http.post('/api/users/register', user);
  }

  public delete(id) {
    return this.http.delete(`api/users/${id}`);
  }

  public getTeacherInfo(userId: string): Observable<TeacherModel> {
    return this.http.get<TeacherModel>(`api/users/${userId}`);
  }


  public switchRole(id: string, role: string): Observable<UserDataModel> {
    return this.http.put<UserDataModel>(`api/users/${id}/${role}`, null);
  }
}
