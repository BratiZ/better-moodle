export function addOrDeleteItem<T>(array: T[], item: T): boolean {
  const itemIndex = array.indexOf(item);

  if (itemIndex > -1) {
    array.splice(itemIndex, 1);
  } else {
    array.push(item);
  }

  return true;
}
