import {Injectable} from '@angular/core';
import {DatePipe} from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class TimeUtils {

  constructor(
    private datePipe: DatePipe
  ) {
  }

  public dateToString(date: Date): string {
    return this.datePipe.transform(date, 'yyyy-MM-dd');
  }

  public dateToCalendarString(date: Date): string {
    return this.datePipe.transform(date, 'M/d/yyyy');
  }

}
