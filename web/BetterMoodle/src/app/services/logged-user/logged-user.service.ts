import {Injectable} from '@angular/core';
import {AuthorizationService} from '../authorization/authorization.service';

@Injectable({
  providedIn: 'root'
})
export class LoggedUserService {
  constructor(
    private authorizationService: AuthorizationService,
  ) {
  }

  public getUserPersonalityType() {
    return this.authorizationService.getUserType();
  }

  public setUserPersonalityType(name: string) {
    return this.authorizationService.setUserType(name);
  }

  public hasUserPersonalityType(): boolean {
    return this.getUserPersonalityType() !== 'NOTYPE';
  }
}
