import {UserRoles} from '../../enums/user-roles';
import {Injectable} from '@angular/core';
import {AuthorizationService} from '../authorization/authorization.service';

@Injectable({
  providedIn: 'root'
})
export class PermissionsService {

  constructor(
    private authorizationService: AuthorizationService,
  ) {
  }

  public hasTeacherPermission(): boolean {
    return this.hasPermission(UserRoles.TEACHER);
  }

  public hasAdminPermission(): boolean {
    return this.hasPermission(UserRoles.ADMIN);
  }

  public hasPermission(requiredRole: UserRoles) {
    const userRole = this.authorizationService.getUserRole();

    return userRole === UserRoles.ADMIN || userRole === requiredRole;
  }
}
