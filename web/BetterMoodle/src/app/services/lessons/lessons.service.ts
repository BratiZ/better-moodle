import {Injectable} from '@angular/core';
import {AnswerModel, AnswerResultModel, ContentType, CourseLesson, CourseLessonSolve, LessonContent, LessonItemType} from '../../models/courses/lessons.model';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {first, mergeMap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LessonsService {

  constructor(
    private httpClient: HttpClient,
  ) {
  }

  public getLessonsForCourse(courseId: number): Observable<CourseLesson[]> {
    return this.httpClient.get<CourseLesson[]>(`api/courses/${courseId}/lessons`);
  }

  public getLesson(courseId: number, lessonId: number): Observable<CourseLesson> {
    return this.getLessonsForCourse(courseId)
      .pipe(
        mergeMap(lesson => lesson),
        // tslint:disable-next-line:triple-equals
        first((lesson: CourseLesson) => lesson.id == lessonId)
      );
  }

  public getContentForLesson(courseId: number, lessonId: number): Observable<LessonContent[]> {
    return this.httpClient.get<LessonContent[]>(`api/courses/${courseId}/lessons/${lessonId}`);
  }

  public addLessonsForCourse(courseId: number, lessonName: string): Observable<any> {
    return this.httpClient.post(`api/courses/${courseId}/lessons?name=${lessonName}`, {});
  }

  public deleteLessonForCourse(courseId: number, lessonId: number): Observable<any> {
    return this.httpClient.delete(`api/courses/${courseId}/lessons/${lessonId}`);
  }

  public changeLessonName(courseId: number, lessonId: number, newTitle: string): Observable<string> {
    return this.httpClient.put<string>(`api/courses/${courseId}/lessons/${lessonId}?name=${newTitle}`, null);
  }

  public addItemToLesson(courseId: number, lessonId: number, contentType: ContentType, item: LessonItemType): Observable<LessonItemType> {
    return this.httpClient.post<LessonItemType>(`api/courses/${courseId}/lessons/${lessonId}/${contentType.toLowerCase()}`, item);
  }

  public deleteItemFromLesson(itemId: number, courseId: number): Observable<string> {
    return this.httpClient.delete<string>(`api/courses/${courseId}/lessons/item/${itemId}`);
  }

  public getLessonsForCourseSolve(courseId: number): Observable<CourseLessonSolve[]> {
    return this.httpClient.get<CourseLessonSolve[]>(`api/courses/${courseId}/lessons/solve`);
  }

  public sendQuestionAnswersForLesson(courseId: number, lessonId: number, userAnswers: AnswerModel[]) {
    return this.httpClient.post<AnswerResultModel>(`api/courses/${courseId}/lessons/${lessonId}/solve`, userAnswers);
  }
}
