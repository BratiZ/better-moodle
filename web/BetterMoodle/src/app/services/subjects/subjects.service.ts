import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {SubjectEditModel, SubjectInfoModel} from '../../models/subject/subject.model';

@Injectable({
  providedIn: 'root'
})
export class SubjectsService {

  constructor(
    private http: HttpClient
  ) {
  }

  public getAvailableSubjects(): Observable<SubjectInfoModel[]> {
    return this.http.get<SubjectInfoModel[]>('api/subjects');
  }

  public getAllSubjects(): Observable<SubjectInfoModel[]> {
    return this.http.get<SubjectInfoModel[]>('api/subjects');
  }

  public deleteSubject(subjectId: string): Observable<string> {
    return this.http.put<string>(`api/subjects/${subjectId}/delete`, null);
  }

  public createNewSubject(subjectName: string): Observable<string> {
    return this.http.post<string>(`api/subjects/${subjectName}`, null);
  }

  public unarchiveSubject(subjectId: string): Observable<string> {
    return this.http.put<string>(`api/subjects/${subjectId}/unarchive`, null);
  }

  public editSubjectName(subjectEditModel: SubjectEditModel): Observable<SubjectInfoModel> {
    return this.http.put<SubjectInfoModel>('api/subjects/editname', subjectEditModel);
  }
}
