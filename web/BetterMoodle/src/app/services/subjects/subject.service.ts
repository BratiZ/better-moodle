import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {SubjectInfoModel} from '../../models/subject/subject.model';

@Injectable({
  providedIn: 'root'
})
export class SubjectService {
  constructor(
    private http: HttpClient
  ) {
  }

  public getSubjectInfo(subjectId: string): Observable<SubjectInfoModel> {
    return this.http.get<SubjectInfoModel>(`/api/subjects/${subjectId}/info`);
  }
}
