import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {QuestionnaireContainerComponent} from './components/questionnaire/questionnaire-container/questionnaire-container.component';
import {HomeComponent} from './components/home/home.component';
import {AuthorizationComponent} from './components/authorization/authorization.component';
import {RegistrationComponent} from './components/registration/registration.component';
import {AuthorizationGuard} from './guards/authorization/authorization.guard';
import {HomeworkComponent} from './components/homework/homework.component';
import {CoursesComponent} from './components/courses/courses_list/courses.component';
import {SubjectComponent} from './components/subjects/subject/subject.component';
import {CoursesDataComponent} from './components/courses/courses_data/courses-data.component';
import {SubjectsComponent} from './components/subjects/subjects/subjects.component';
import {CourseEditComponent} from './components/courses/course_edit/course-edit.component';
import {CourseSolveComponent} from './components/courses/course_solve/course-solve.component';
import {AdminPanelComponent} from './components/admin-panel/admin-panel.component';


const routes: Routes = [

  {
    path: 'courses',
    component: CoursesComponent,
    canActivate: [AuthorizationGuard]
  },

  {
    path: 'courses/:key',
    component: CoursesDataComponent,
    canActivate: [AuthorizationGuard]
  },
  {
    path: 'courses/:key/solve',
    component: CourseSolveComponent,
    canActivate: [AuthorizationGuard]
  },

  {
    path: 'courses/:key/edit',
    component: CourseEditComponent,
    canActivate: [AuthorizationGuard]
  },

  {
    path: 'homework',
    component: HomeworkComponent,
    canActivate: [AuthorizationGuard]
  },

  {
    path: 'home',
    component: HomeComponent,
    canActivate: [AuthorizationGuard]
  },

  {
    path: 'questionnaire/:key',
    component: QuestionnaireContainerComponent,
    canActivate: [AuthorizationGuard]
  },

  {
    path: 'subjects',
    component: SubjectsComponent,
    canActivate: [AuthorizationGuard]
  },

  {
    path: 'subjects/:key',
    component: SubjectComponent,
    canActivate: [AuthorizationGuard]
  },

  {
    path: 'usermanagement',
    component: AdminPanelComponent,
    canActivate: [AuthorizationGuard]
  },

  {
    path: 'login',
    component: AuthorizationComponent
  },

  {
    path: 'registration',
    component: RegistrationComponent
  },

  {
    path: '*',
    redirectTo: '/home'
  },
  {
    path: '**',
    redirectTo: '/home'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
