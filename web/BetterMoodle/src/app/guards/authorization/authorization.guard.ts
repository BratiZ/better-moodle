import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthorizationService} from '../../services/authorization/authorization.service';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationGuard implements CanActivate {

  constructor(
    private authorizationService: AuthorizationService,
    private router: Router
  ) {
  }

  canActivate() {
    if (!this.authorizationService.isLoggedIn()) {
      this.router.navigate(['/login']);
    }

    return this.authorizationService.isLoggedIn();
  }

}
