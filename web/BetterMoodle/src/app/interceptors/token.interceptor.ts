import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {AuthorizationService} from '../services/authorization/authorization.service';
import {filter, switchMap, take} from 'rxjs/operators';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  private isRefreshing = false;

  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(public authorizationService: AuthorizationService) {
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(this.addToken(request, this.authorizationService.getJwtToken()));
    // .pipe(
    //   catchError(error => {
    //     if (error instanceof Htt1111111pErrorResponse && error.status === 401) {
    //       return this.handle401Error(request, next);
    //     } else {
    //       return throwError(error);
    //     }
    //   }));
  }

  private addToken(request: HttpRequest<any>, token: string): HttpRequest<any> {

    return request.clone({
        withCredentials: true,
        headers: request.headers.set('Authorization', `Bearer ${token}`)
      }
    );
  }

  private handle401Error(request: HttpRequest<any>, next: HttpHandler) {
    if (!this.isRefreshing) {
      this.isRefreshing = true;
      this.refreshTokenSubject.next(null);

      return this.authorizationService.refreshToken()
        .pipe(
          switchMap((token: any) => {
            this.isRefreshing = false;
            this.refreshTokenSubject.next(token.jwt);
            return next.handle(this.addToken(request, token.jwt));
          }));
    } else {
      return this.refreshTokenSubject
        .pipe(
          filter(token => token != null),
          take(1),
          switchMap(jwt => {
            return next.handle(this.addToken(request, jwt));
          }));
    }
  }
}
