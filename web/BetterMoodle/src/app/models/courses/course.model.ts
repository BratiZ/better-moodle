export interface CourseModel {
  courseId: string;
  name: string;
  subject: string;
  subjectId: string;
  isArchived: boolean;
}

export class CourseCreateModel {
  courseName: string;
  subjectId: string;
  startDate: string;
  endDate: string;
}

export interface CourseListModel extends CourseModel {
  startDate: Date;
  endDate: Date;
}

export interface CourseDataModel {
  courseId: string;
  name: string;
  description?: string;
  subjectId: string;
  maxPoints: number;
  ownerId: string;
  questionsCount: number;
  start: Date;
  end: Date;
  archived?: boolean;
}

export interface EditedCourseModel {
  name: string;
  subjectId: number;
  startDate: string;
  endDate: string;
}
