export interface CourseLesson {
  id: number;
  name: string;
}

export interface CourseLessonSolve {
  id: number;
  name: string;
  state: string;
}

export enum LessonState {
  FAIL = 'FAIL',
  UNFINISHED = 'UNFINISHED',
  PASSED = 'PASSED'
}

export interface LessonContent {
  id?: number;
  position: number;
  item: LessonItemType;
  type: ContentType;
}

export type LessonItemType = TextItem | QuestionItem;

export enum ContentType {
  TEXT = 'TEXT',
  QUESTION = 'QUESTION'
}

export interface TextItem {
  id: number;
  title: string;
  content: string[];
}

export interface QuestionItem {
  id: number;
  question: string;
  answers: QuestionItemAnswers[];
  multiAnswer?: boolean;
}

export interface QuestionItemAnswers {
  id: number;
  answer: string;
  isCorrect: boolean;
}

export interface AnswerModel {
  questionId: number;
  answers: number[];
}

export interface AnswerResultModel {
  lessonId: number;
  lessonState: LessonState;
  userPoints: number;
  maxPoints: number;
}
