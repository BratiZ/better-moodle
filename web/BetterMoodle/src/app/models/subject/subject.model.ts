export interface SubjectInfoModel {
  subjectId: string;
  subjectName: string;
  isArchived: boolean;
}

export interface SubjectEditModel {
  id: string;
  name: string;
}
