export interface UserQuestionnaireModel {
  questionnaireKey: string;
  lng: string;

  answers: UserAnswersModel[];
}

export interface UserAnswersModel {
  nr: number;
  type: string;

  answers: string[];
}
