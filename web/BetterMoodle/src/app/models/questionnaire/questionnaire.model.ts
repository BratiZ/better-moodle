import {QuestionBaseModel} from './questions/question-base.model';

export interface QuestionnaireModel {
  name: string;
  language: string;
  questions: QuestionBaseModel[];
}
