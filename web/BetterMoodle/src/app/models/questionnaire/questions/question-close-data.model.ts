export interface QuestionCloseDataModel {
  answers: ClosedAnswerModel[];
  oneAnswerCorrect: boolean;
}

export interface ClosedAnswerModel {
  key: string;
  text: string;
  isCorrect?: boolean;
}
