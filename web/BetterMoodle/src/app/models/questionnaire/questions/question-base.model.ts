import {QuestionTypeEnum} from '../../../enums/question-type.enum';
import {QuestionOpenDataModel} from './question-open-data.model';
import {QuestionCloseDataModel} from './question-close-data.model';
import {QuestionLevelColorEnum} from '../../../enums/question-level-color.enum';

export interface QuestionBaseModel {
  nr: number;
  type: QuestionTypeEnum;
  points?: number;
  description: string;
  difficulty?: QuestionLevelColorEnum;

  openTypeData?: QuestionOpenDataModel;
  closeTypeData?: QuestionCloseDataModel;
}
