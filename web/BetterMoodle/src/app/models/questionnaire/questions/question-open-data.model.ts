export interface QuestionOpenDataModel {
  hasAnswer: boolean;
  answer: string;
}
