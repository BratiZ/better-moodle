export class UserPersonalityTypeModel {
  id: number;
  name: string;
  key: string;
}

export interface TeacherModel {
  teacherId: string;
  name: string;
  email: string;
}

export interface UserDataModel {
  id: number;
  login: string;
  role: string;
}
