export class HttpJwtResponseModel {
  jwt: string;
  refreshToken: string;
  userRole: string;
  userType: string;
}
