import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {TopMenuComponent} from './components/top-menu/top-menu.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './material-module';
import {MatIconModule} from '@angular/material/icon';
import {QuestionnaireContainerComponent} from './components/questionnaire/questionnaire-container/questionnaire-container.component';
import {QuestionClosedComponent} from './components/questionnaire/questions/question-closed/question-closed.component';
import {QuestionOpenComponent} from './components/questionnaire/questions/question-open/question-open.component';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {QuestionBaseComponent} from './components/questionnaire/questions/question-base/question-base.component';
import {QuestionClosedAnswersComponent} from './components/questionnaire/questions/question-closed/answers/multiply-answers/question-closed-answers.component';
import {QuestionClosedOneAnswerCorrectComponent} from './components/questionnaire/questions/question-closed/answers/one-answer/question-closed-one-answer-correct.component';
import {HomeComponent} from './components/home/home.component';
import {AuthorizationComponent} from './components/authorization/authorization.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RegistrationComponent} from './components/registration/registration.component';
import {TokenInterceptor} from './interceptors/token.interceptor';
import {HomeworkComponent} from './components/homework/homework.component';
import {HomeHomeworkComponent} from './components/home/items/home_homework/home-homework.component';
import {HomeItemContainerComponent} from './components/home/items/home_item_container/home-item-container.component';
import {HomeItemCoursesComponent} from './components/home/items/home_item_courses/home-item-courses.component';
import {CoursesComponent} from './components/courses/courses_list/courses.component';
import {HomeAlertsComponent} from './components/home/items/home-alerts/home-alerts.component';
import {DatePipe} from '@angular/common';
import {SubjectComponent} from './components/subjects/subject/subject.component';
import {CoursesDataComponent} from './components/courses/courses_data/courses-data.component';
import {SubjectsComponent} from './components/subjects/subjects/subjects.component';
import {CourseEditComponent} from './components/courses/course_edit/course-edit.component';
import {HomeItemSubjectsComponent} from './components/home/items/home-item-subjects/home-item-subjects.component';
import {CourseBaseComponent} from './components/courses/course_base/course-base.component';
import {CourseListVerticalComponent} from './components/courses/course_lesson_list/course-list-vertical.component';
import {CourseSolveComponent} from './components/courses/course_solve/course-solve.component';
import {MultiInputModalComponent} from './components/modals/mult_input/multi-input-modal.component';
import {CourseLessonListEditComponent} from './components/courses/course_lesson_list_edit/course-lesson-list-edit.component';
import {CoursesService} from './services/courses/courses.service';
import {CourseItemTextComponent} from './components/courses/course_edit/course-item-text/course-item-text.component';
import {CourseItemQuestionComponent} from './components/courses/course_edit/course-item-question/course-item-question.component';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import {CourseItemQuestionSolveComponent} from './components/courses/course_solve/course-item-question-solve/course-item-question-solve.component';
import {AdminPanelComponent} from './components/admin-panel/admin-panel.component';
import {CourseItemTextSolveInnerComponent} from './components/courses/course_solve/course_item_text_solve/course-item-text-solve-inner/course-item-text-solve-inner.component';
import {CourseItemTextSolveComponent} from './components/courses/course_solve/course_item_text_solve/course-item-text-solve.component';
import {SpeechSynthesisModule,} from '@kamiazya/ngx-speech-synthesis';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    TopMenuComponent,
    QuestionnaireContainerComponent,
    QuestionClosedComponent,
    QuestionOpenComponent,
    QuestionBaseComponent,
    QuestionClosedAnswersComponent,
    QuestionClosedOneAnswerCorrectComponent,
    HomeComponent,
    AuthorizationComponent,
    RegistrationComponent,
    HomeworkComponent,
    HomeHomeworkComponent,
    HomeItemContainerComponent,
    HomeItemCoursesComponent,
    CoursesComponent,
    HomeAlertsComponent,
    SubjectComponent,
    CoursesDataComponent,
    SubjectsComponent,
    CourseEditComponent,
    HomeItemSubjectsComponent,
    CourseBaseComponent,
    CourseListVerticalComponent,
    CourseSolveComponent,
    MultiInputModalComponent,
    CourseLessonListEditComponent,
    CourseItemTextComponent,
    CourseItemQuestionComponent,
    CourseItemTextSolveComponent,
    CourseItemQuestionSolveComponent,
    AdminPanelComponent,
    CourseItemTextSolveInnerComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
    MatIconModule,
    CKEditorModule,
    SpeechSynthesisModule.forRoot({
      lang: 'en',
      volume: 1.0,
      pitch: 1.0,
      rate: 1.0,
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      },
      defaultLanguage: 'en'
    }),
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    DatePipe,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    CoursesService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
