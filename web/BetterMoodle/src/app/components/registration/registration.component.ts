import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import {Router} from '@angular/router';
import {AuthorizationService} from '../../services/authorization/authorization.service';
import {UserServiceService} from '../../services/user/user-service.service';
import {RegistrationService} from '../../services/registration/registration.service';
import {HttpErrorResponse} from '@angular/common/http';
import {finalize} from 'rxjs/operators';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})

export class RegistrationComponent implements OnInit {
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
  registerForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;
  isRegistrationComplete: boolean = true;
  visibleSpinner: boolean = false;
  isPassMatch: boolean = true;
  isUsernameEmpty: boolean = true;
  isEmailEmpty: boolean = true;
  isPasswordEmpty: boolean = true;


  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private authorizationService: AuthorizationService,
              private userService: UserServiceService,
              private registrationService: RegistrationService) {
  }

  ngOnInit(): void {

  }


  register(login: string, email: string, password: string, repeatedPass: string) {
    this.visibleSpinner = true;

    if (this.ok(login, email, password, repeatedPass)) {
      this.registrationService.register({login, email, password})
        .pipe(
          finalize(() => this.visibleSpinner = false)
        )
        .subscribe(
          (x) => {
            this.router.navigate(['/home']);
          },
          (y: HttpErrorResponse) => alert(y.error)// todo modal
        );
    }

  }

  ok(login: string, email: string, password: string, repeatedPass: string) {
    if (this.loginEmpty(login) || this.emailEmpty(email) || this.passwordEmpty(password) || this.passDoesntMatch(password, repeatedPass)) {
      return false;
    }

    return true;
  }

  passDoesntMatch(password: string, repeatedPass: string) {
    if (password !== repeatedPass) {
      this.isPassMatch = false;
      return true;
    }
    return false;
  }

  loginEmpty(login: string) {
    this.isUsernameEmpty = true;
    if (login === '') {
      this.isUsernameEmpty = false;
      return true;
    }
    return false;
  }

  emailEmpty(email: string) {
    this.isEmailEmpty = true;
    if (email === '') {
      this.isEmailEmpty = false;
      return true;
    }
    return false;
  }

  passwordEmpty(password: string) {
    this.isPasswordEmpty = true;
    if (password === '') {
      this.isPasswordEmpty = false;
      return true;
    }
    return false;
  }

}
