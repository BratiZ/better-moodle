import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionClosedComponent } from './question-closed.component';

describe('QuestionClosedComponent', () => {
  let component: QuestionClosedComponent;
  let fixture: ComponentFixture<QuestionClosedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionClosedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionClosedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
