import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionClosedAnswersComponent } from './question-closed-answers.component';

describe('QuestionClosedAnswersComponent', () => {
  let component: QuestionClosedAnswersComponent;
  let fixture: ComponentFixture<QuestionClosedAnswersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionClosedAnswersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionClosedAnswersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
