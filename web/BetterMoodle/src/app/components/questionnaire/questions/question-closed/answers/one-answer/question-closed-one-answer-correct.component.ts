import {Component, Input, OnInit} from '@angular/core';
import {ClosedAnswerModel} from '../../../../../../models/questionnaire/questions/question-close-data.model';

@Component({
  selector: 'app-closed-question-answer-multi',
  templateUrl: './question-closed-one-answer-correct.component.html',
  styleUrls: ['./question-closed-one-answer-correct.component.css']
})
export class QuestionClosedOneAnswerCorrectComponent implements OnInit {
  @Input('data') answers: ClosedAnswerModel[];
  @Input() userAnswerData: {};
  @Input() questionId: {};

  private questionKey = 'Q_CLOSE_ONE_';

  constructor() {
  }

  ngOnInit(): void {
  }

  onChange(key: string): void {
    this.userAnswerData[this.questionKey + this.questionId] = key;
  }

}
