import {Component, Input, OnInit} from '@angular/core';
import {ClosedAnswerModel} from '../../../../../../models/questionnaire/questions/question-close-data.model';
import {addOrDeleteItem} from '../../../../../../services/utils/array-utils';

@Component({
  selector: 'app-closed-question-answer',
  templateUrl: './question-closed-answers.component.html',
  styleUrls: ['./question-closed-answers.component.css']
})
export class QuestionClosedAnswersComponent implements OnInit {
  @Input('data') answers: ClosedAnswerModel[];
  @Input() userAnswerData: {};
  @Input() questionId: number;

  private questionKey: string = 'Q_CLOSE_MULTI_';
  private keysOfSelectedAnswers: string[] = [];

  constructor() {
  }

  ngOnInit(): void {
  }

  onChange(checkedKey: string): void {
    addOrDeleteItem(this.keysOfSelectedAnswers, checkedKey);
    this.userAnswerData[this.questionKey + this.questionId] = this.keysOfSelectedAnswers;
  }


}
