import {Component, Input, OnInit} from '@angular/core';
import {QuestionCloseDataModel} from '../../../../models/questionnaire/questions/question-close-data.model';

@Component({
  selector: 'app-question-closed',
  templateUrl: './question-closed.component.html',
  styleUrls: ['./question-closed.component.css']
})
export class QuestionClosedComponent implements OnInit {

  @Input('data') questionData: QuestionCloseDataModel;
  @Input() questionId: number;
  @Input() userAnswerData: {};

  constructor() {
  }

  ngOnInit(): void {
  }

}
