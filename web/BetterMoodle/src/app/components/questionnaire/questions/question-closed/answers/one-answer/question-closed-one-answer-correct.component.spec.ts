import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionClosedOneAnswerCorrectComponent } from './question-closed-one-answer-correct.component';

describe('QuestionClosedOneAnswerCorrectComponent', () => {
  let component: QuestionClosedOneAnswerCorrectComponent;
  let fixture: ComponentFixture<QuestionClosedOneAnswerCorrectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionClosedOneAnswerCorrectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionClosedOneAnswerCorrectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
