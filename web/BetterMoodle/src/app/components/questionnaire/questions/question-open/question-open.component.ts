import {Component, Input, OnInit} from '@angular/core';
import {QuestionOpenDataModel} from '../../../../models/questionnaire/questions/question-open-data.model';

@Component({
  selector: 'app-question-open',
  templateUrl: './question-open.component.html',
  styleUrls: ['./question-open.component.css']
})
export class QuestionOpenComponent implements OnInit {

  @Input('data') questionData: QuestionOpenDataModel;
  @Input() questionId: number;
  @Input() userAnswerData: {};

  private questionKey: string = 'Q_OPEN_';

  constructor() {
  }

  ngOnInit(): void {
  }

  onChangeEvent(value: string) {
    this.userAnswerData[this.questionKey + this.questionId] = value;
  }
}
