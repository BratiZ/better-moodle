import {Component, Input, OnInit} from '@angular/core';
import {QuestionLevelColorEnum} from '../../../../enums/question-level-color.enum';
import {QuestionBaseModel} from '../../../../models/questionnaire/questions/question-base.model';
import {QuestionTypeEnum} from '../../../../enums/question-type.enum';

@Component({
  selector: 'app-question',
  templateUrl: './question-base.component.html',
  styleUrls: ['./question-base.component.css']
})
export class QuestionBaseComponent implements OnInit {
  @Input() question: QuestionBaseModel;
  @Input() userAnswerData: {};

  questionTypes = QuestionTypeEnum;

  constructor() {
  }

  ngOnInit(): void {
  }

  getDifficultyColor() {
    return 'background-color: ' + (this.question.difficulty || QuestionLevelColorEnum.LVL_0);
  }


}
