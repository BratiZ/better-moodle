import {Component, OnInit} from '@angular/core';
import {QuestionBaseModel} from '../../../models/questionnaire/questions/question-base.model';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, Router} from '@angular/router';
import {QuestionnaireService} from '../../../services/questionnaire/questionnaire.service';
import {UserAnswersModel, UserQuestionnaireModel} from '../../../models/questionnaire/useranswers/user-questionnaire.model';
import {LoggedUserService} from '../../../services/logged-user/logged-user.service';

@Component({
  selector: 'app-questionnaire-container',
  templateUrl: './questionnaire-container.component.html',
  styleUrls: ['./questionnaire-container.component.css']
})
export class QuestionnaireContainerComponent implements OnInit {
  questions: QuestionBaseModel[] = [];
  questionnaireName: string = '';
  questionnaireKey: string = '';
  questionnaireLang: string = '';
  userAnswerData: {} = {};
  loading: boolean = false;

  questionOpenKey: string = 'Q_OPEN_';
  questionOneCorrectKey: string = 'Q_CLOSE_ONE_';
  questionMultiKey: string = 'Q_CLOSE_MULTI_';

  constructor(
    private translate: TranslateService,
    private router: Router,
    private activeRouter: ActivatedRoute,
    private questionnaireService: QuestionnaireService,
    private loggedUserService: LoggedUserService,
  ) {
  }

  ngOnInit(): void {
    // @ts-ignore
    this.questionnaireKey = this.activeRouter.params.value.key;

    this.loading = true;

    this.questionnaireService.getQuestionnaire(this.questionnaireKey)
      .then(
        res => {
          this.questions = res.questions;
          this.questionnaireName = res.name;
          this.questionnaireLang = res.language;
        }
      )
      .finally(() => this.loading = false);
  }

  public safelyRedirectToHomePage() {
    this.translate.get('app.confirmation.page.leave', 'en')
      .subscribe(
        (x: string) => {
          if (confirm(x)) {
            console.warn('redirect to home page');
            this.redirectToHome();
          }
        }).unsubscribe();

  }

  private redirectToHome() {
    this.router.navigate(['/home']);
  }

  public collectAndSendAnswers() {
    const answers: UserQuestionnaireModel = this.mapAnswers();

    return this.questionnaireService.sendAnswersFromQuestionnaire(answers)
      .subscribe(
        response => {
          this.loggedUserService.setUserPersonalityType(response.key);
          console.log(response.key);
          this.openDialogSucc(response.name);
        },
        error => this.openDialogErr(error.toString())
      );
  }

  private mapAnswers(): UserQuestionnaireModel {
    const keys = Object.keys(this.userAnswerData);

    const mappedAnswers: UserQuestionnaireModel = {
      questionnaireKey: this.questionnaireKey,
      lng: this.translate.getBrowserLang(),
      answers: []
    };

    keys.forEach(
      key => {
        mappedAnswers.answers.push(this.mapQuestionToModel(key));
      }
    );

    return mappedAnswers;
  }

  private mapQuestionToModel(key: string): UserAnswersModel {

    if (key.startsWith(this.questionOpenKey)) {
      return this.mapToOpenQuestionModel(key);
    }

    if (key.startsWith(this.questionOneCorrectKey)) {
      return this.mapToClosedQuestionOneAnswerModel(key);
    }

    if (key.startsWith(this.questionMultiKey)) {
      return this.mapToClosedQuestionMultiAnswerModel(key);
    }

    console.error('cannot map key: ' + key);
    return null;
  }

  private mapToOpenQuestionModel(key: string): UserAnswersModel {
    const answer: UserAnswersModel = {
      nr: this.extractOpenQuestionNumberFromKey(key),
      type: 'OPEN',
      answers: []
    };

    answer.answers.push(this.userAnswerData[key]);

    return answer;
  }

  private mapToClosedQuestionOneAnswerModel(key: string): UserAnswersModel {
    const answer: UserAnswersModel = {
      nr: this.extractCloseQuestionOneAnswerNumberFromKey(key),
      type: 'CLOSE',
      answers: []
    };

    answer.answers.push(this.userAnswerData[key]);

    return answer;
  }

  private mapToClosedQuestionMultiAnswerModel(key: string): UserAnswersModel {
    const answer: UserAnswersModel = {
      nr: this.extractCloseQuestionMultiAnswerNumberFromKey(key),
      type: 'CLOSE',
      answers: []
    };

    answer.answers.push(this.userAnswerData[key]);

    return answer;
  }

  private extractOpenQuestionNumberFromKey(key: string): number {
    return +key.substr(this.questionOpenKey.length);
  }

  private extractCloseQuestionOneAnswerNumberFromKey(key: string): number {
    return +key.substr(this.questionOneCorrectKey.length);
  }

  private extractCloseQuestionMultiAnswerNumberFromKey(key: string): number {
    return +key.substr(this.questionMultiKey.length);
  }

  openDialogSucc(name: string) { // todo modal
    alert('Twój typ to: ' + name); // todo i18n
    this.redirectToHome();
  }

  openDialogErr(error: string) { // todo modal
    alert('Wystąpił błąd: ' + name); // todo i18n
  }
}
