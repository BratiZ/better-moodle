import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {AuthorizationService} from '../../services/authorization/authorization.service';
import {Router} from '@angular/router';
import {PermissionsService} from '../../services/permissions/permissions-service';

@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TopMenuComponent implements OnInit {

  showMenu: boolean = true;

  constructor(
    private authorizationService: AuthorizationService,
    private router: Router,
    private permissionsService: PermissionsService
  ) {
  }

  ngOnInit(): void {
  }

  isLoggedIn() {
    return this.authorizationService.isLoggedIn();
  }

  logout() {
    this.authorizationService.doLogoutUser();
    this.router.navigate(['/login']);
  }

  menuVisibleClick() {
    this.showMenu = !this.showMenu;
  }
  public hasAdminPermission(): boolean {
    return this.permissionsService.hasAdminPermission();
  }
}
