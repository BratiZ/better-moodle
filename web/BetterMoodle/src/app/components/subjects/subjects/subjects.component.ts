import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {TimeUtils} from '../../../services/utils/time-utils';
import {PermissionsService} from '../../../services/permissions/permissions-service';
import {ActivatedRoute, Router} from '@angular/router';
import {SubjectsService} from '../../../services/subjects/subjects.service';
import {finalize} from 'rxjs/operators';
import {SubjectInfoModel} from '../../../models/subject/subject.model';
import {MultiInputModalComponent, MultiInputModalData, MultiInputModalResult} from '../../modals/mult_input/multi-input-modal.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-subjects',
  templateUrl: './subjects.component.html',
  styleUrls: ['./subjects.component.css']
})
export class SubjectsComponent implements OnInit {
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  displayedColumns: string[] = ['subjectName', 'xxx'];
  dataSource: MatTableDataSource<SubjectInfoModel>;
  showNewSubjectRow: boolean = false;
  queryFilter: string = '';

  private getModalData(): MultiInputModalData {
    return {
      title: 'Edit Subject Name',
      content: [
        {
          labelKey: 'subjectName',
          value: '',
          label: 'Subject name'
        }
      ]
    };
  }

  constructor(
    private subjectsService: SubjectsService,
    private timeUtils: TimeUtils,
    private permissionsService: PermissionsService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog
  ) {
  }

  ngOnInit(): void {
    this.subjectsService.getAllSubjects()
      .pipe(
        finalize(() => this.setValueFromQueryParam()),
        finalize(() => this.dataSource.sort = this.sort),
      )
      .subscribe(
        (subjectModes: SubjectInfoModel[]) => {
          this.dataSource = new MatTableDataSource(subjectModes);
        },
        error => {
          alert('Cannot get all subject - error in console'); // todo modal
          console.error(error);
        }
      );
  }

  private setValueFromQueryParam(): void {
    this.activatedRoute.queryParams
      .subscribe(params => {
        this.queryFilter = params.filter || '';
        this.makeFilter(this.queryFilter);
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.makeFilter(filterValue);
  }

  private makeFilter(value: string) {
    this.dataSource.filter = value.trim().toLowerCase();
    this.router.navigate(
      [],
      {
        relativeTo: this.activatedRoute,
        queryParams: {filter: value},
        queryParamsHandling: 'merge',
      }
    );
  }

  public hasTeacherPermission(): boolean {
    return this.permissionsService.hasTeacherPermission();
  }

  clearFilter() {
    this.makeFilter('');
  }

  public toggleNewCourseRow(): void {
    this.showNewSubjectRow = !this.showNewSubjectRow;
  }

  deleteSubject(subjectId: string) {
    this.subjectsService.deleteSubject(subjectId)
      .subscribe(
        success => {
          alert(`Subject ${subjectId} deleted`);
          this.refreshPage();

        }, // todo modal
        error => {
          alert(`cannot delete course with ${subjectId}`);
          console.error(error);
        }
      );
  }

  public addNewSubject(subjectName: string): void {
    this.subjectsService.createNewSubject(subjectName)
      .pipe(
        finalize(() => this.showNewSubjectRow = false),
      )
      .subscribe(
        success => {
          alert('Subject created'); // todo modal
          this.refreshPage();
        },
        error => {
          alert('Cannot create subject - error in console ' + subjectName); // todo modal
          console.error(error);
        }
      );
  }

  private refreshPage(): void {
    window.location.reload();
  }

  unarchiveSubject(subjectId: any) {
    this.subjectsService.unarchiveSubject(subjectId)
      .subscribe(
        success => {
          alert(`Course ${subjectId} unarchived`);
          this.refreshPage();
        }, // todo modal

        error => {
          alert(`cannot unarchive subject with ${subjectId}`);
          console.error(error);
        }
      );
  }

  public openEditSubjectDialog(subjectId: string): void {
    const dialogRef = this.dialog.open(
      MultiInputModalComponent,
      {
        data: this.getModalData()
      }
    );

    dialogRef.afterClosed()
      .subscribe((result: MultiInputModalResult[]) => this.parseData(result, subjectId));
  }

  private parseData(result: MultiInputModalResult[], subjectId:string) {
    if (!result) {
      return;
    }

    result.filter(res => res.labelKey === 'subjectName')
      .forEach(
        res => this.subjectsService.editSubjectName({id:subjectId, name:res.value})
          .pipe(
            finalize(() => this.refreshPage())
          )
          .subscribe()
      );
  }
}
