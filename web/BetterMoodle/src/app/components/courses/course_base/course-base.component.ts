import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CoursesService} from '../../../services/courses/courses.service';
import {SubjectInfoModel} from '../../../models/subject/subject.model';
import {SubjectsService} from '../../../services/subjects/subjects.service';
import {CourseDataModel} from '../../../models/courses/course.model';
import {TimeUtils} from '../../../services/utils/time-utils';
import {LoggedUserService} from '../../../services/logged-user/logged-user.service';
import {PermissionsService} from '../../../services/permissions/permissions-service';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'app-course-base',
  templateUrl: './course-base.component.html',
  styleUrls: ['./course-base.component.css']
})
export class CourseBaseComponent implements OnInit {
  @Input('courseId') courseId: string;
  @Input('editMode') editMode: boolean;

  course: CourseDataModel;
  subjects: SubjectInfoModel[];
  showEditView = false;

  constructor(
    private router: Router,
    private coursesService: CoursesService,
    private subjectsService: SubjectsService,
    private timeUtils: TimeUtils,
    private loggedUserService: LoggedUserService,
    private permissionsService: PermissionsService,
  ) {
  }

  loadData = false;
  isValid = false;

  ngOnInit(): void {
    this.coursesService.getCourseInfo(this.courseId)
      .pipe(
        finalize(() => {
          this.isValid = this.validateAccess();
          this.loadData = this.isValid;
          this.getSubjects();
        }),
      )
      .subscribe(
        res => this.course = res
      );
  }

  private getSubjects() {
    this.subjectsService.getAllSubjects()
      .subscribe(
        (subjectModels: SubjectInfoModel[]) => {
          this.subjects = subjectModels;
        },
        error => {
          alert('Cannot get all subjects - error in console'); // todo modal
          console.error(error);
        }
      );
  }

  private validateAccess(): boolean {
    if (this.isCourseDeleted()) {
      alert('Oh crap! Course was recently deleted. Contact with administrator if you need access (:');
      this.router.navigate([`/courses`]);
      return false;
    }

    if (this.isCourseEnded()) {
      alert('Oh crap! Course has recently ended. Contact with teacher if you need access (:');
      this.router.navigate([`/courses/${this.courseId}`]);
      return false;
    }

    if (this.shouldRedirectToQuestionnaire()) {
      alert('Oh crap! You didnt finish personality questionnaire yet! \nPlease finnish it, so we can display all our data in best way for you (:');
      this.router.navigate(['/questionnaire/Personality']);
      return false;

    }

    return true;
  }

  private shouldRedirectToQuestionnaire(): boolean {
    return !this.editMode && (!this.permissionsService.hasTeacherPermission() && !this.loggedUserService.hasUserPersonalityType());
  }

  private isCourseEnded(): boolean {
    const date: Date = new Date(this.course?.end);
    const nowDate: Date = new Date(Date.now());

    const isEnded = date.setHours(0, 0, 0, 0) <= nowDate.setHours(0, 0, 0, 0);

    return !this.permissionsService.hasTeacherPermission() && isEnded;
  }

  private isCourseDeleted(): boolean {
    return this.course.archived;
  }

  redirectToCourse() {
    this.router.navigate([`/courses/${this.courseId}`]);
  }

  public formatDateToString(date: Date): string {
    return this.timeUtils.dateToCalendarString(date);
  }

  addNewCourse(name: string, subjectId: any, startDate: string, endDate: string) {
    this.coursesService.editCourse(this.courseId, {
      name,
      subjectId,
      startDate,
      endDate
    }).subscribe(
      res => this.course = res
    );
  }

  public showEditToggle(): void {
    this.showEditView = !this.showEditView;
  }
}
