import {Component, Input, OnInit} from '@angular/core';
import {MultiInputModalComponent, MultiInputModalData, MultiInputModalResult} from '../../modals/mult_input/multi-input-modal.component';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {CourseLesson} from '../../../models/courses/lessons.model';
import {CoursesService} from '../../../services/courses/courses.service';
import {LessonsService} from '../../../services/lessons/lessons.service';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'app-course-lesson-list-edit',
  templateUrl: 'course-lesson-list-edit.component.html',
  styleUrls: ['./course-lesson-list-edit.component.css']
})
export class CourseLessonListEditComponent implements OnInit {
  @Input('courseId') courseId: number;

  courseLessonList: CourseLesson[];

  activeLesson: number;

  private getModalData(): MultiInputModalData {
    return {
      title: 'Add new lesson',
      content: [
        {
          labelKey: 'lessonName',
          value: '',
          label: 'Lesson name'
        }
      ]
    };
  }

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private coursesService: CoursesService,
    private lessonsService: LessonsService,
    public dialog: MatDialog,
  ) {
  }

  ngOnInit(): void {
    this.getCourseList();
  }

  public redirectToLesson(id: number): void {
    this.activeLesson = id;
    this.router.navigate(
      [],
      {
        relativeTo: this.activatedRoute,
        queryParams: {lessonId: id},
        queryParamsHandling: 'merge',
      }
    );
  }

  public openAddNewLessonDialog(): void {
    const dialogRef = this.dialog.open(
      MultiInputModalComponent,
      {
        data: this.getModalData()
      }
    );

    dialogRef.afterClosed()
      .subscribe((result: MultiInputModalResult[]) => this.parseData(result));
  }

  private parseData(result: MultiInputModalResult[]) {
    if (!result) {
      return;
    }

    result.filter(res => res.labelKey === 'lessonName')
      .forEach(
        res => this.lessonsService.addLessonsForCourse(this.courseId, res.value)
          .pipe(
            finalize(() => this.getCourseList())
          )
          .subscribe()
      );
  }

  public deleteLesson(id: number): void {
    this.lessonsService.deleteLessonForCourse(this.courseId, id)
      .pipe(
        finalize(() => {
          this.getCourseList();
          if (this.activeLesson == id) {
            this.redirectToLesson(null);
          }
        })
      )
      .subscribe();
  }

  private getCourseList(): void {
    this.lessonsService.getLessonsForCourse(this.courseId)
      .subscribe(
        (response: CourseLesson[]) => {
          this.courseLessonList = response;
        },
        error => {
          alert('cannot get lessons for course ' + this.courseId);
          console.error(error);
        }
      );
  }
}
