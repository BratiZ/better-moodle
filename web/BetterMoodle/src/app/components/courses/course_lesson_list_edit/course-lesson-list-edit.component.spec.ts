import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseLessonListEditComponent } from './course-lesson-list-edit.component';

describe('CourseLessonListEditComponent', () => {
  let component: CourseLessonListEditComponent;
  let fixture: ComponentFixture<CourseLessonListEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseLessonListEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseLessonListEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
