import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseItemQuestionComponent } from './course-item-question.component';

describe('CourseItemQuestionComponent', () => {
  let component: CourseItemQuestionComponent;
  let fixture: ComponentFixture<CourseItemQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseItemQuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseItemQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
