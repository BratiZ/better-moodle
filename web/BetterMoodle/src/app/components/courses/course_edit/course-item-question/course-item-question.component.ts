import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {QuestionItem} from '../../../../models/courses/lessons.model';

@Component({
  selector: 'app-course-item-question',
  templateUrl: './course-item-question.component.html',
  styleUrls: ['./course-item-question.component.css']
})
export class CourseItemQuestionComponent implements OnInit {

  @Output()
  onSave: EventEmitter<QuestionItem> = new EventEmitter<QuestionItem>();
  @Output()
  onDelete: EventEmitter<any> = new EventEmitter<any>();
  @Input('lessonQuestionModel') question: QuestionItem;

  title = new FormControl('', [Validators.required]);

  changed: boolean = false;
  newQuestionsId = -1;

  getErrorMessage() {
    if (this.title.hasError('required')) {
      return 'You must enter a value';
    }
  }

  constructor() {
  }

  ngOnInit(): void {
  }

  onSaveClick() {
    this.onSave.emit({
      id: this.question.id,
      question: this.question.question,
      answers: this.question.answers,
      multiAnswer: !!this.question.multiAnswer,
    });

    this.changed = false;
  }

  onDeleteClick() {
    this.onDelete.emit(this.question.id);
  }

  onChange() {
    this.changed = true;
  }

  multiAnswerToggle() {
    this.question.multiAnswer = !this.question.multiAnswer;

    if (!this.question.multiAnswer) {
      this.disableAllValuesWhenIsMoreThanOneCorrect();
    }
  }

  private disableAllValuesWhenIsMoreThanOneCorrect() {
    const correctAns = this.question.answers.filter(ans => ans.isCorrect).length;

    if (correctAns > 1) {
      this.question.answers.forEach(ans => ans.isCorrect = false);
    }
  }

  removeAnswer(id: number) {
    this.question.answers = this.question.answers.filter(answer => answer.id !== id);
  }

  checkboxCorrectChange(id: number) {
    this.question.answers.find(answer => answer.id === id).isCorrect
      = !this.question.answers.find(answer => answer.id === id).isCorrect;
  }

  radioCorrectChange(id: number) {
    this.question.answers.forEach(ans => ans.isCorrect = false);

    this.question.answers.find(answer => answer.id === id).isCorrect
      = !this.question.answers.find(answer => answer.id === id).isCorrect;
  }

  addNewAnswer(answer: string) {
    this.question.answers.push({
      id: this.newQuestionsId,
      answer,
      isCorrect: false,
    });

    this.newQuestionsId -= 1;
  }
}
