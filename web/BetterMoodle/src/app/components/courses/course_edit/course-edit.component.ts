import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ContentType, CourseLesson, LessonContent, LessonItemType, QuestionItem, TextItem} from '../../../models/courses/lessons.model';
import {CoursesService} from '../../../services/courses/courses.service';
import {LessonsService} from '../../../services/lessons/lessons.service';
import {finalize} from 'rxjs/operators';

@Component({
  selector: 'app-course-edit',
  templateUrl: './course-edit.component.html',
  styleUrls: ['./course-edit.component.css']
})
export class CourseEditComponent implements OnInit {
  courseId: number;
  lessonId: number;
  lessonInfo: CourseLesson;
  titleChanged: boolean = false;

  lessonContent: LessonContent[];

  constructor(
    private activatedRoute: ActivatedRoute,
    private coursesService: CoursesService,
    private lessonsService: LessonsService,
  ) {
  }

  ngOnInit(): void {
    // @ts-ignore
    this.courseId = this.activatedRoute.params.value.key;
    this.setValueFromQueryParam();
  }

  private setValueFromQueryParam(): void {
    this.activatedRoute.queryParams
      .subscribe(params => {
        this.lessonId = params && params.lessonId || '';
        if (this.lessonId) {
          this.getLessonInfo();
          this.getLessonContent();
        }
      });
  }

  private getLessonInfo() {
    this.lessonsService.getLesson(this.courseId, this.lessonId)
      .subscribe(
        (response: CourseLesson) => {
          this.lessonInfo = response;
        },
        error => {
          alert(`cannot get lesson info for courseId: ${this.courseId} lessonId: ${this.lessonId}`);
          console.error(error);
        }
      );
  }

  private getLessonContent() {
    this.lessonsService.getContentForLesson(this.courseId, this.lessonId)
      .subscribe(
        res => this.lessonContent = res
      );
  }

  onItemSaveText(item: TextItem): LessonItemType {
    let res: LessonItemType;

    this.lessonsService.addItemToLesson(this.courseId, this.lessonId, ContentType.TEXT, item)
      .pipe(
        finalize(() => this.getLessonContent())
      )
      .subscribe(
        (newItem: LessonItemType) => {
          res = newItem;
          alert('Added new item to lesson');
        },
        err => {
          alert('Cannot add item to lesson');
          console.error(err);
        }
      );

    return {...item};
  }

  onItemSaveQuestion(item: LessonItemType): LessonItemType {
    let res: LessonItemType;

    this.lessonsService.addItemToLesson(this.courseId, this.lessonId, ContentType.QUESTION, item)
      .pipe(
        finalize(() => this.getLessonContent())
      )
      .subscribe(
        (newItem: LessonItemType) => {
          res = newItem;
          alert('Added new item to lesson');
        },
        err => {
          alert('Cannot add item to lesson');
          console.error(err);
        }
      );

    return {...item};
  }

  onItemDelete(id: number) {
    this.lessonsService.deleteItemFromLesson(id, this.courseId)
      .pipe(
        finalize(() => this.getLessonContent())
      )
      .subscribe(
        res => alert('Item deleted from lesson'),
        err => {
          alert('cannot delete item from lesson');
          console.error(err);
        }
      );
  }

  onLessonTitleSave(newTitle: string) {
    this.lessonsService.changeLessonName(this.courseId, this.lessonId, newTitle)
      .pipe(
        finalize(() => this.refreshPage())
      )
      .subscribe(
        succ => alert('Lesson title changed'),
        err => {
          alert('Cannot change lesson title');
          console.error(err);
        }
      );
  }

  onLessonTitleChangeToggle() {
    this.titleChanged = true;
  }


  addNewTextItem() {

    const newLessonItem: TextItem = {
      id: -1,
      title: '',
      content: [],
    };

    this.lessonContent.push({
      position: -1,
      type: ContentType.TEXT,
      item: newLessonItem,
    });
  }

  addNewQuestionItem() {

    const newLessonItem: QuestionItem = {
      id: -1,
      question: '',
      multiAnswer: false,
      answers: []
    };

    this.lessonContent.push({
      position: -1,
      type: ContentType.QUESTION,
      item: newLessonItem,
    });
  }

  private refreshPage(): void {
    window.location.reload();
  }
}
