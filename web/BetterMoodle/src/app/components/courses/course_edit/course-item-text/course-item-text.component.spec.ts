import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseItemTextComponent } from './course-item-text.component';

describe('CourseItemTextComponent', () => {
  let component: CourseItemTextComponent;
  let fixture: ComponentFixture<CourseItemTextComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseItemTextComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseItemTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
