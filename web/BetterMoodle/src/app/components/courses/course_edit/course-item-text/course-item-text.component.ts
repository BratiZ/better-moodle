import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {FormControl, Validators} from '@angular/forms';
import {TextItem} from '../../../../models/courses/lessons.model';

@Component({
  selector: 'app-course-item-text',
  templateUrl: './course-item-text.component.html',
  styleUrls: ['./course-item-text.component.css']
})
export class CourseItemTextComponent implements OnInit {

  @Input('lessonTextModel') lesson: TextItem;

  @Output()
  onSave: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  onDelete: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('myEditor') myEditor: any;
  public Editor = ClassicEditor;

  title = new FormControl('', [Validators.required]);

  data: string = '';
  changed: boolean = false;

  getErrorMessage() {
    if (this.title.hasError('required')) {
      return 'You must enter a value';
    }
  }

  constructor() {
  }

  ngOnInit(): void {
    ClassicEditor.defaultConfig = {
      toolbar: {
        items: [
          'heading',
          '|',
          'bold',
          'italic',
          'link',
          '|',
          'bulletedList',
          'numberedList',
          '|',
          'insertTable',
          '|',
          'undo',
          'redo'
        ]
      },
      image: {
        toolbar: [
          'imageStyle:full',
          'imageStyle:side',
          '|',
          'imageTextAlternative'
        ]
      },
      table: {
        contentToolbar: ['tableColumn', 'tableRow', 'mergeTableCells']
      },
      language: 'en'
    };

    this.data = this.lesson.content.join('');
  }

  onSaveClick() {
    this.onSave.emit({
      id: this.lesson.id,
      title: this.lesson.title,
      content: [this.getArticleContent()],
    });

    this.data = this.lesson.content.join('');
    this.myEditor.editorInstance.data.set(this.data);
    this.changed = false;
  }

  onDeleteClick() {
    this.onDelete.emit(this.lesson.id);
  }

  private getArticleContent(): string {
    if (this.myEditor && this.myEditor.editorInstance) {
      return this.myEditor.editorInstance.getData();
    }

    return '';
  }

  onChange() {
    this.changed = true;
  }
}
