import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {PermissionsService} from '../../../services/permissions/permissions-service';
import {CoursesService} from '../../../services/courses/courses.service';
import {CourseDataModel} from '../../../models/courses/course.model';
import {TeacherModel} from '../../../models/userdata/user.model';
import {SubjectInfoModel} from '../../../models/subject/subject.model';
import {mergeMap, shareReplay} from 'rxjs/operators';
import {UserServiceService} from '../../../services/user/user-service.service';
import {SubjectService} from '../../../services/subjects/subject.service';
import {TimeUtils} from '../../../services/utils/time-utils';

@Component({
  selector: 'app-courses-data',
  templateUrl: './courses-data.component.html',
  styleUrls: ['./courses-data.component.css']
})
export class CoursesDataComponent implements OnInit {
  courseId: string = '';

  courseDataModel$: Observable<CourseDataModel>;
  teacherDataModel$: Observable<TeacherModel>;
  subjectDataModel$: Observable<SubjectInfoModel>;

  stars: number[] = [1, 2, 3, 4, 5];
  selectedValue: number;

  constructor(
    private activeRouter: ActivatedRoute,
    private permissionsService: PermissionsService,
    private coursesService: CoursesService,
    private userServiceService: UserServiceService,
    private subjectService: SubjectService,
    private router: Router,
    private timeUtils: TimeUtils,
  ) {
  }

  public ngOnInit(): void {
    // @ts-ignore
    this.courseId = this.activeRouter.params.value.key;

    this.courseDataModel$ = this.coursesService.getCourseInfo(this.courseId)
      .pipe(
        shareReplay(),
      );

    this.teacherDataModel$ = this.courseDataModel$
      .pipe(
        mergeMap(courseModel => {
          return this.userServiceService.getTeacherInfo(courseModel.ownerId);
        }),
        shareReplay(),
      );

    this.subjectDataModel$ = this.courseDataModel$
      .pipe(
        mergeMap(courseModel => this.subjectService.getSubjectInfo(courseModel.subjectId)),
        shareReplay(),
      );
  }

  public hasTeacherPermission = () => this.permissionsService.hasTeacherPermission();

  public deleteCourse(): void {
    this.coursesService.deleteCourse(this.courseId)
      .subscribe(
        success => alert(`Course ${this.courseId} deleted`), // todo modal
        error => {
          alert(`cannot delete course with ${this.courseId}`);
          console.error(error);
        }
      );
  }

  public stopCourse(): void {
    this.coursesService.stopCourse(this.courseId)
      .subscribe(
        success => {
          alert(`Course stopped`);
          this.router.navigate([`/courses`]);
        }, // todo modal
        error => {
          alert(`cannot stop course with`);
          console.error(error);
        }
      );
  }

  public countStar(star): void {
    this.selectedValue = star;
  }

  public formatTime(time: Date): string {
    return this.timeUtils.dateToString(time);
  }

  public redirectToSubject(subjectId): void {
    this.router.navigate([`/subjects/${subjectId}`]);
  }
}
