import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseSolveComponent } from './course-solve.component';

describe('CourseSolveComponent', () => {
  let component: CourseSolveComponent;
  let fixture: ComponentFixture<CourseSolveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseSolveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseSolveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
