import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {LoggedUserService} from '../../../../../services/logged-user/logged-user.service';

@Component({
  selector: 'app-course-item-text-solve-inner',
  templateUrl: './course-item-text-solve-inner.component.html',
  encapsulation: ViewEncapsulation.ShadowDom,
  styleUrls: ['./course-item-text-solve-inner.component.css']
})
export class CourseItemTextSolveInnerComponent implements OnInit {
  @Input('content') content: string;

  constructor(
    private loggedUserService: LoggedUserService,
  ) {
  }

  ngOnInit(): void {
  }

  public isVisual(): boolean {
    return this.loggedUserService.getUserPersonalityType() === 'VISUAL';
  }

}
