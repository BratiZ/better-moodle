import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseItemTextSolveComponent } from './course-item-text-solve.component';

describe('CourseItemTextSolveComponent', () => {
  let component: CourseItemTextSolveComponent;
  let fixture: ComponentFixture<CourseItemTextSolveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseItemTextSolveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseItemTextSolveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
