import {Component, Input, OnInit} from '@angular/core';
import {TextItem} from '../../../../models/courses/lessons.model';
import {PermissionsService} from '../../../../services/permissions/permissions-service';
import {LoggedUserService} from '../../../../services/logged-user/logged-user.service';
import {SpeechSynthesisService, SpeechSynthesisUtteranceFactoryService} from '@kamiazya/ngx-speech-synthesis';

@Component({
  selector: 'app-course-item-text-solve',
  templateUrl: './course-item-text-solve.component.html',
  styleUrls: ['./course-item-text-solve.component.css']
})
export class CourseItemTextSolveComponent implements OnInit {
  @Input('lessonTextModel') lesson: TextItem;
  raw: boolean = false;
  contents: string[] = [];
  speaking: boolean = false;

  constructor(
    private permissionsService: PermissionsService,
    private loggedUserService: LoggedUserService,
    public f: SpeechSynthesisUtteranceFactoryService,
    public svc: SpeechSynthesisService,
  ) {
  }

  ngOnInit(): void {
    if (this.lesson?.content) {
      this.contents = this.lesson.content;
    }
  }

  public rawToggle() {
    this.raw = !this.raw;
  }

  public hasAdminPermission(): boolean {
    return this.permissionsService.hasAdminPermission();
  }

  public getUserType(): string {
    return this.loggedUserService.getUserPersonalityType();
  }

  speech() {
    this.speaking = true;
    for (const text of this.contents) {
      const v = this.f.text(text);
      this.svc.speak(this.f.text(text));
    }
  }

  cancel() {
    this.speaking = false;
    this.svc.cancel();
  }

  pause() {
    this.svc.pause();
  }

  resume() {
    this.svc.resume();
  }
}
