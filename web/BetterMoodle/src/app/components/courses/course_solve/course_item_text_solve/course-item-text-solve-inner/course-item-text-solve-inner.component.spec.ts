import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseItemTextSolveInnerComponent } from './course-item-text-solve-inner.component';

describe('CourseItemTextSolveInnerComponent', () => {
  let component: CourseItemTextSolveInnerComponent;
  let fixture: ComponentFixture<CourseItemTextSolveInnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseItemTextSolveInnerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseItemTextSolveInnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
