import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AnswerModel, AnswerResultModel, ContentType, CourseLesson, LessonContent, LessonState, QuestionItem} from '../../../models/courses/lessons.model';
import {LessonsService} from '../../../services/lessons/lessons.service';
import {tap} from 'rxjs/operators';

@Component({
  selector: 'app-course-solve',
  templateUrl: './course-solve.component.html',
  styleUrls: ['./course-solve.component.css']
})
export class CourseSolveComponent implements OnInit {

  constructor(
    private activatedRoute: ActivatedRoute,
    private lessonsService: LessonsService,
  ) {
  }

  courseId: number;
  lessonId: number;
  lessonInfo: CourseLesson;
  lessonContent: LessonContent[];

  private userAnswers: AnswerModel[];

  ngOnInit(): void {
    // @ts-ignore
    this.courseId = this.activatedRoute.params.value.key;
    this.setValueFromQueryParam();
  }

  private setValueFromQueryParam(): void {
    this.activatedRoute.queryParams
      .pipe(
        tap(() => this.userAnswers = [])
      )
      .subscribe(params => {
        this.lessonId = params && params.lessonId || '';
        if (this.lessonId) {
          this.getLessonInfo();
          this.getLessonContent();
        }
      });
  }

  private getLessonInfo() {
    this.lessonsService.getLesson(this.courseId, this.lessonId)
      .subscribe(
        (response: CourseLesson) => {
          this.lessonInfo = response;
        },
        error => {
          alert(`cannot get lesson info for courseId: ${this.courseId} lessonId: ${this.lessonId}`);
          console.error(error);
        }
      );
  }

  private getLessonContent() {
    this.lessonsService.getContentForLesson(this.courseId, this.lessonId)
      .subscribe(
        res => this.lessonContent = res
      );
  }

  onAnswer(newAnswer: AnswerModel) {
    this.userAnswers = this.userAnswers.filter(answer => answer.questionId !== newAnswer.questionId);
    this.userAnswers.push(newAnswer);
    this.removeEmptyUserAnswers();
  }

  private removeEmptyUserAnswers() {
    this.userAnswers = this.userAnswers.filter(answer => answer.answers.length !== 0);
  }

  onSave() {
    const answers = this.lessonContent.filter(content => content.type === ContentType.QUESTION);
    const questionsWithoutAnswer = answers.filter(ans => !this.userAnswers.map(answer => answer.questionId).includes(ans.item.id));

    if (questionsWithoutAnswer.length > 0) {
      const parsedQuestionNames = questionsWithoutAnswer.map(ans => ans.item as QuestionItem).map(ans => ans.question).map(question => `* ${question}\n`).join('');
      alert(`You have to answer for all questions!\nQuestion(s) without answer:\n${parsedQuestionNames}`);
      return;
    }

    this.lessonsService.sendQuestionAnswersForLesson(this.courseId, this.lessonId, this.userAnswers)
      .subscribe(
        (result: AnswerResultModel) => {
          if (result.lessonState === LessonState.FAIL) {
            alert(`Unfortunately, you didnt do it ):\nYour result: ${result.lessonState}\nPoints: ${result.userPoints}/${result.maxPoints}`);
            window.location.reload();
            return;
          }

          alert(`Yay! You did it (:\nYour result: ${result.lessonState}\nPoints: ${result.userPoints}/${result.maxPoints}`);
          window.location.reload();
        }
      );
  }
}
