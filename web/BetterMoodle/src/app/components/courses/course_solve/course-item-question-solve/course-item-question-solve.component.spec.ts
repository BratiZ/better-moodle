import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseItemQuestionSolveComponent } from './course-item-question-solve.component';

describe('CourseItemQuestionSolveComponent', () => {
  let component: CourseItemQuestionSolveComponent;
  let fixture: ComponentFixture<CourseItemQuestionSolveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseItemQuestionSolveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseItemQuestionSolveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
