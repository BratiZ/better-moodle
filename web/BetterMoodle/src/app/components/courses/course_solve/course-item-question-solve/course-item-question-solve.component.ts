import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AnswerModel, QuestionItem} from '../../../../models/courses/lessons.model';

@Component({
  selector: 'app-course-item-question-solve',
  templateUrl: './course-item-question-solve.component.html',
  styleUrls: ['./course-item-question-solve.component.css']
})
export class CourseItemQuestionSolveComponent implements OnInit {

  constructor() {
  }

  @Input('content') content: QuestionItem;

  @Output()
  answered: EventEmitter<AnswerModel> = new EventEmitter<AnswerModel>();

  private checkboxValues: number[] = [];

  ngOnInit(): void {
  }

  public onQuestionEditRadioButton(answerId: number): void {
    this.answered.emit({
      questionId: this.content.id,
      answers: [answerId]
    });
  }

  public onQuestionEditCheckbox(answerId: number): void {
    this.addToCheckboxValues(answerId);

    this.answered.emit({
      questionId: this.content.id,
      answers: this.checkboxValues
    });
  }

  private addToCheckboxValues(answerId: number): void {
    if (this.checkboxValues.includes(answerId)) {
      this.checkboxValues = this.checkboxValues.filter(answer => answer !== answerId);
      return;
    }
    this.checkboxValues.push(answerId);
  }
}
