import {Component, OnInit, ViewChild} from '@angular/core';
import {CoursesService} from '../../../services/courses/courses.service';
import {CourseListModel} from '../../../models/courses/course.model';
import {MatTableDataSource} from '@angular/material/table';
import {TimeUtils} from '../../../services/utils/time-utils';
import {MatSort} from '@angular/material/sort';
import {PermissionsService} from '../../../services/permissions/permissions-service';
import {ActivatedRoute, Router} from '@angular/router';
import {finalize} from 'rxjs/operators';
import {SubjectsService} from '../../../services/subjects/subjects.service';
import {SubjectInfoModel} from '../../../models/subject/subject.model';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css'],
})
export class CoursesComponent implements OnInit {
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  displayedColumns: string[] = ['name', 'subject', 'startDate', 'endDate', 'buttons'];
  dataSource: MatTableDataSource<CourseListModel>;
  showNewCourseRow: boolean = false;
  queryFilter: string = '';
  subjects: SubjectInfoModel[];

  constructor(
    private coursesService: CoursesService,
    private subjectsService: SubjectsService,
    private timeUtils: TimeUtils,
    private permissionsService: PermissionsService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.coursesService.getAllCourses()
      .pipe(
        finalize(() => this.setValueFromQueryParam()),
        finalize(() => this.dataSource.sort = this.sort),
      )
      .subscribe(
        (courseModes: CourseListModel[]) => {
          this.dataSource = new MatTableDataSource(courseModes);
        },
        error => {
          alert('Cannot get all curses - error in console'); // todo modal
          console.error(error);
        }
      );

    this.subjectsService.getAllSubjects()
      .subscribe(
        (subjectModels: SubjectInfoModel[]) => {
          this.subjects = subjectModels;
        },
        error => {
          alert('Cannot get all subjects - error in console'); // todo modal
          console.error(error);
        }
      );
  }

  private setValueFromQueryParam(): void {
    this.activatedRoute.queryParams
      .subscribe(params => {
        this.queryFilter = params.filter || '';
        this.makeFilter(this.queryFilter);
      });
  }

  public applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.makeFilter(filterValue);
  }

  private makeFilter(value: string) {
    this.dataSource.filter = value.trim().toLowerCase();
    this.router.navigate(
      [],
      {
        relativeTo: this.activatedRoute,
        queryParams: {filter: value},
        queryParamsHandling: 'merge',
      }
    );
  }

  public formatDateToString(date: Date): string {
    return this.timeUtils.dateToString(date);
  }

  public stopCourse(courseId: string): void {
    this.coursesService.stopCourse(courseId)
      .subscribe(
        (success) => {
          alert(`Course stopped`);
          this.refreshPage();
        }, // todo modal
        (error) => {
          console.error(error);
          alert(`cannot stop course with ${courseId}`);
        }
      );
  }

  public deleteCourse(courseId: string): void {
    this.coursesService.deleteCourse(courseId)
      .subscribe(
        success => {
          alert(`Course ${courseId} deleted`);
          this.refreshPage();

        }, // todo modal
        error => {
          alert(`cannot delete course with ${courseId}`);
          console.error(error);
        }
      );
  }

  public hasTeacherPermission(): boolean {
    return this.permissionsService.hasTeacherPermission();
  }

  public toggleNewCourseRow(): void {
    this.showNewCourseRow = !this.showNewCourseRow;
  }

  public addNewCourse(courseName: string, subjectId: string, startDate: string, endDate: string): void {
    this.coursesService.createNewCourse({courseName, subjectId, startDate, endDate})
      .pipe(
        finalize(() => this.showNewCourseRow = false),
      )
      .subscribe(
        success => {
          alert('Course created'); // todo modal
          this.refreshPage();
        },
        error => {
          alert('Cannot create course - error in console'); // todo modal
          console.error(error);
        }
      );
  }

  public unarchiveCourse(courseId: string): void {
    this.coursesService.unarchiveCourse(courseId)
      .subscribe(
        success => {
          alert(`Course ${courseId} unarchived`);
          this.refreshPage();
        }, // todo modal

        error => {
          alert(`cannot unarchive course with ${courseId}`);
          console.error(error);
        }
      );
  }

  private refreshPage(): void {
    window.location.reload();
  }

  public clearFilter(): void {
    this.makeFilter('');
  }

  isCourseEnded(endDate: string): boolean {
    const date: Date = new Date(endDate);
    const nowDate: Date = new Date(Date.now());

    return date.setHours(0, 0, 0, 0) <= nowDate.setHours(0, 0, 0, 0);
  }
}
