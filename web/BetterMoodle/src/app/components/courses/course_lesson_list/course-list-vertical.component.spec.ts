import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseListVerticalComponent } from './course-list-vertical.component';

describe('CourseListVerticalComponent', () => {
  let component: CourseListVerticalComponent;
  let fixture: ComponentFixture<CourseListVerticalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseListVerticalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseListVerticalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
