import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CourseLesson, CourseLessonSolve, LessonState} from '../../../models/courses/lessons.model';
import {CoursesService} from '../../../services/courses/courses.service';
import {LessonsService} from '../../../services/lessons/lessons.service';
import {tap} from 'rxjs/operators';


@Component({
  selector: 'app-course-list-vertical',
  templateUrl: './course-list-vertical.component.html',
  styleUrls: ['./course-list-vertical.component.css']
})
export class CourseListVerticalComponent implements OnInit {
  @Input('courseId') courseId: number;
  @Input('editMode') editMode: boolean;

  courseLessonList: CourseLessonSolve[];

  showPassed: boolean = true;
  showUnfinished: boolean = true;
  showFail: boolean = true;

  activeLesson: number;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private coursesService: CoursesService,
    private lessonsService: LessonsService,
  ) {
  }

  ngOnInit(): void {
    this.getCourseList();
  }

  public redirectToLesson(id: number): void {
    this.activeLesson = id;
    this.router.navigate(
      [],
      {
        relativeTo: this.activatedRoute,
        queryParams: {lessonId: id},
        queryParamsHandling: 'merge',
      }
    );
  }

  public isActive(state: string): boolean {
    return state === LessonState.PASSED && this.showPassed
      || state === LessonState.UNFINISHED && this.showUnfinished
      || state === LessonState.FAIL && this.showFail;
  }

  public activeItems(): CourseLessonSolve[] {
    return (this.courseLessonList || []).filter(lesson => this.isActive(lesson.state));
  }

  private getCourseList(): void {
    this.lessonsService.getLessonsForCourseSolve(this.courseId)
      .subscribe(
        (response: CourseLessonSolve[]) => {
          this.courseLessonList = response;
        },
        error => {
          alert('cannot get lessons for course ' + this.courseId);
          console.error(error);
        }
      );
  }
}
