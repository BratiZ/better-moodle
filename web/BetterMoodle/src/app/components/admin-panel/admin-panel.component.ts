import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {SubjectInfoModel} from '../../models/subject/subject.model';
import {PermissionsService} from '../../services/permissions/permissions-service';
import {ActivatedRoute, Router} from '@angular/router';
import {finalize} from 'rxjs/operators';
import {UserServiceService} from '../../services/user/user-service.service';
import {UserDataModel} from '../../models/userdata/user.model';
import {RegistrationService} from '../../services/registration/registration.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css']
})
export class AdminPanelComponent implements OnInit {
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  displayedColumns: string[] = ['userName', 'xxx'];
  dataSource: MatTableDataSource<UserDataModel>;
  showNewSubjectRow: boolean = false;
  queryFilter: string = '';

  constructor(
    private permissionsService: PermissionsService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private userService: UserServiceService,
    private registrationService: RegistrationService
  ) {

  }

  ngOnInit(): void {
    this.userService.getAll()
      .pipe(
        finalize(() => this.setValueFromQueryParam()),
        finalize(() => this.dataSource.sort = this.sort),
      )
      .subscribe(
        (userModel: UserDataModel[]) => {
          this.dataSource = new MatTableDataSource(userModel);
        },
        error => {
          alert('Cannot get all users - error in console'); // todo modal
          console.error(error);
        }
      );
  }

  private setValueFromQueryParam(): void {
    this.activatedRoute.queryParams
      .subscribe(params => {
        this.queryFilter = params.filter || '';
        this.makeFilter(this.queryFilter);
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.makeFilter(filterValue);
  }

  private makeFilter(value: string) {
    this.dataSource.filter = value.trim().toLowerCase();
    this.router.navigate(
      [],
      {
        relativeTo: this.activatedRoute,
        queryParams: {filter: value},
        queryParamsHandling: 'merge',
      }
    );
  }

  clearFilter() {
    this.makeFilter('');
  }

  public toggleNewCourseRow(): void {
    this.showNewSubjectRow = !this.showNewSubjectRow;
  }

  public hasTeacherPermission(): boolean {
    return this.permissionsService.hasTeacherPermission();
  }

  addNewNewUser(login: string, password: string, email: string) {
    this.registrationService.register({login, email, password})
      .subscribe(
        (x) => {
          this.refreshPage();
        },
        (y: HttpErrorResponse) => alert(y.error)// todo modal
      );
  }

  switchToUser(id: string) {
    this.userService.switchRole(id, 'USER')
      .subscribe(
        success => {
          alert(`Switched role`);
          this.refreshPage();
        }, // todo modal

        error => {
          alert(`cannot switch`);
          console.error(error);
        }
      );
  }


  switchToTeacher(id: string) {
    this.userService.switchRole(id, 'TEACHER')
      .subscribe(
        success => {
          alert(`Switched role`);
          this.refreshPage();
        }, // todo modal

        error => {
          alert(`cannot switch`);
          console.error(error);
        }
      );
  }


  switchToAdmin(id: string) {
    this.userService.switchRole(id, 'ADMIN')
      .subscribe(
        success => {
          alert(`Switched role`);
          this.refreshPage();
        }, // todo modal

        error => {
          alert(`cannot switch`);
          console.error(error);
        }
      );
  }

  private refreshPage(): void {
    window.location.reload();
  }
}
