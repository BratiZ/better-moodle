import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

export class MultiInputModalData {
  title: string;
  content: MultiInputModalDataContent[];
}

export class MultiInputModalDataContent {
  labelKey: string;
  label: string;
  value: string;
}

export class MultiInputModalResult {
  labelKey: string;
  value: string;
}

@Component({
  selector: 'app-multi-input-modal',
  templateUrl: 'multi-input-modal.component.html',
  styleUrls: ['./multi-input-modal.component.css'],
})
export class MultiInputModalComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<MultiInputModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: MultiInputModalData,
  ) {
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
  }

  public returnInputs(): MultiInputModalResult[] {
    return this.data.content.map(
      input => this.mapContentItemToResult(input)
    );
  }

  private mapContentItemToResult(item: MultiInputModalDataContent): MultiInputModalResult {
    return {
      labelKey: item.labelKey,
      value: item.value
    };
  }
}
