import {Component, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'app-home-homework',
  templateUrl: './home-homework.component.html',
  styleUrls: ['./home-homework.component.css']
})
export class HomeHomeworkComponent implements OnInit {

  @ViewChild('myChild') element;

  constructor() {
  }

  ngOnInit(): void {
  }

}
