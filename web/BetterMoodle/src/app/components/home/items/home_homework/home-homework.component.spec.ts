import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeHomeworkComponent } from './home-homework.component';

describe('HomeHomeworkComponent', () => {
  let component: HomeHomeworkComponent;
  let fixture: ComponentFixture<HomeHomeworkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeHomeworkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeHomeworkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
