import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {SubjectsService} from '../../../../services/subjects/subjects.service';
import {SubjectInfoModel} from '../../../../models/subject/subject.model';


@Component({
  selector: 'app-home-item-subjects',
  templateUrl: './home-item-subjects.component.html',
  styleUrls: ['./home-item-subjects.component.css']
})
export class HomeItemSubjectsComponent implements OnInit {

  public subjectsModels: SubjectInfoModel[];

  constructor(
    private subjectsService: SubjectsService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.subjectsService.getAvailableSubjects()
      .subscribe(
        (models: SubjectInfoModel[]) => {
          this.subjectsModels = models;
        },
        error => {
          alert('Error while getting available subjects for user');
          console.error(error);
        }
      );
  }

  public redirectToSubject(subjectId: string): void {
    this.router.navigate([`/subjects/${subjectId}`]);
  }
}
