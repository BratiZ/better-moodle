import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeItemSubjectsComponent } from './home-item-subjects.component';

describe('HomeItemSubjectsComponent', () => {
  let component: HomeItemSubjectsComponent;
  let fixture: ComponentFixture<HomeItemSubjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeItemSubjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeItemSubjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
