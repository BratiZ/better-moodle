import {Component, OnInit} from '@angular/core';
import {LoggedUserService} from '../../../../services/logged-user/logged-user.service';
import {PermissionsService} from '../../../../services/permissions/permissions-service';

@Component({
  selector: 'app-home-alerts',
  templateUrl: './home-alerts.component.html',
  styleUrls: ['./home-alerts.component.css']
})
export class HomeAlertsComponent implements OnInit {
  show: boolean = true;
  information: { text: string, link?: { label: string, link: string } }[] = [];

  constructor(
    private loggedUserService: LoggedUserService,
    private permissionsService: PermissionsService,
  ) {
  }

  ngOnInit(): void {
    if (!this.permissionsService.hasTeacherPermission() && !this.loggedUserService.hasUserPersonalityType()) {
      this.information.push({
        text: 'You didnt finish personality questionnaire yet! Please finnish it, so we can display all our data in best way for you (:',
        link: {
          label: 'Questionnaire',
          link: '/questionnaire/Personality'
        }
      });
    }

    if (this.information.length == 0) {
      this.show = false;
    }
  }

  extend() {
    this.show = !this.show;
  }
}
