import {Component, OnInit} from '@angular/core';
import {CourseModel} from '../../../../models/courses/course.model';
import {CoursesService} from '../../../../services/courses/courses.service';
import {Router} from '@angular/router';
import {PermissionsService} from '../../../../services/permissions/permissions-service';

@Component({
  selector: 'app-home-item-courses',
  templateUrl: './home-item-courses.component.html',
  styleUrls: ['./home-item-courses.component.css']
})
export class HomeItemCoursesComponent implements OnInit {

  public courseModels: CourseModel[];

  constructor(
    private coursesService: CoursesService,
    private router: Router,
    private permissionsService: PermissionsService,
  ) {
  }

  ngOnInit(): void {
    this.coursesService.getAvailableCourses()
      .subscribe(
        (models: CourseModel[]) => {
          this.courseModels = models;
        },
        error => {
          alert('Error while getting available curses for user');
          console.error(error);
        }
      );
  }

  redirectToSubject(subjectId: string) {
    this.router.navigate([`/subjects/${subjectId}`]);
  }

  redirectToCourse(courseId: string) {
    this.router.navigate([`/courses/${courseId}`]);
  }

  hasTeacherPermission() {
    return this.permissionsService.hasTeacherPermission();
  }

  stopCourse(courseId: string) {
    this.coursesService.stopCourse(courseId);
  }

  deleteCourse(courseId: string) {
    this.coursesService.deleteCourse(courseId);
  }
}
