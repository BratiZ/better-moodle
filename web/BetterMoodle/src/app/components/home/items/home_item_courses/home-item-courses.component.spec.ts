import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeItemCoursesComponent } from './home-item-courses.component';

describe('HomeItemCoursesComponent', () => {
  let component: HomeItemCoursesComponent;
  let fixture: ComponentFixture<HomeItemCoursesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeItemCoursesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeItemCoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
