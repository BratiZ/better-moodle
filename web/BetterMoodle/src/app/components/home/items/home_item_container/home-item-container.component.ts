import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-home-item-container',
  templateUrl: './home-item-container.component.html',
  styleUrls: ['./home-item-container.component.css']
})
export class HomeItemContainerComponent implements OnInit {

  @Input('title') title: string;
  @Input('link') link: string;

  constructor() {
  }

  ngOnInit(): void {
    if (!this.link) {
      this.link = '/home';
    }

    if (!this.title) {
      this.title = 'NO TITLE!';
    }
  }
}
