import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthorizationService} from '../../services/authorization/authorization.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  qPersonalityKey: string = 'Personality';

  constructor(
    private router: Router,
    private authorizationService: AuthorizationService
  ) {
  }

  ngOnInit(): void {
  }

  goToPersonality() {
    this.router.navigate(['', this.qPersonalityKey]);
  }

  logout() {
    this.authorizationService.doLogoutUser();
    this.router.navigate(['/login']);
  }
}
