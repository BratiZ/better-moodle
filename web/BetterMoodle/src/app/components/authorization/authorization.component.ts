import {Component, HostListener, OnInit} from '@angular/core';
import {AuthorizationService} from '../../services/authorization/authorization.service';
import {finalize} from 'rxjs/operators';
import {Router} from '@angular/router';

@Component({
  selector: 'app-authorization',
  templateUrl: './authorization.component.html',
  styleUrls: ['./authorization.component.css']
})
export class AuthorizationComponent implements OnInit {
  isAuthorizationComplete: boolean = true;
  visibleSpinner: boolean = false;

  constructor(
    private authorizationService: AuthorizationService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
  }

  login(login: string, password: string) {
    this.visibleSpinner = true;

    this.authorizationService.login({login, password})
      .pipe(
        finalize(() => this.visibleSpinner = false)
      )
      .subscribe(resolve => {
        this.isAuthorizationComplete = resolve;

        if (this.isAuthorizationComplete) {
          this.router.navigate(['/home']);
        }
      }, error => {
        console.error(error);
      });
  }

  register() {
    this.router.navigate(['/registration']);
  }

}
