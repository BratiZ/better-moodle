package com.pl.uwb.mgr.r1.aism.moodlePro.database.courses.models;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CourseModel {
    private int id;
    private String name;
    private int subjectId;
    private Date start;
    private Date end;
    private int ownerId;
    private int questionsCount;
    private int maxPoints;
    private boolean isArchived;
}
