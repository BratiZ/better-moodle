package com.pl.uwb.mgr.r1.aism.moodlePro.database.user.models;


import com.pl.uwb.mgr.r1.aism.moodlePro.controllers.questionnaries.enums.UserTypes;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.user.enums.UserRole;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.user.HttpUserRegistrationModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Data
@Getter
@AllArgsConstructor
public class UserModel {
    private int id;
    private String login;
    private String password;
    private String role;
    private String Email;
    private UserTypes userType;

    public static UserModel of(HttpUserRegistrationModel httpUserRegistrationModel) {
        return new UserModel(
                0,
                httpUserRegistrationModel.getLogin(),
                httpUserRegistrationModel.getPassword(),
                UserRole.USER.getKey(),
                httpUserRegistrationModel.getEmail(),
                UserTypes.NOTYPE
        );
    }
}
