package com.pl.uwb.mgr.r1.aism.moodlePro.database.questionnaires.models;

import lombok.Getter;
import java.util.List;

@Getter
public class CloseTypeDataModel {
    Boolean oneAnswerCorrect;
    List<AnswerModel> answers;
}
