package com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.oldModels;

import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.enums.LessonState;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.models.LessonCompleationModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HttpCourseLessonModel {
    private int id;
    private String name;
    private LessonState state;

    public static HttpCourseLessonModel of(LessonCompleationModel lessonModel, String name) {
        return new HttpCourseLessonModel(
                lessonModel.getLessonId(),
                name,
                lessonModel.getLessonState()
        );
    }
}
