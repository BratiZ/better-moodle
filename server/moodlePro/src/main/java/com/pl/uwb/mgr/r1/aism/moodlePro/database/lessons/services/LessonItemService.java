package com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.services;

import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.enums.LessonContentType;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.models.LessonItemTypeModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.models.LessonQuestionItemModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.models.LessonTextItemModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.newModels.HttpCreateLessonQuestionItem;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.newModels.HttpCreateLessonTextItem;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.newModels.HttpNewLessonItemModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.newModels.HttpNewLessonQuestionModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.newModels.HttpNewLessonTextItemModel;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LessonItemService {
    private List<LessonItemTypeModel> items;
    private Integer nextContentId = 19;

    @Autowired
    public LessonItemService() {
        this.items = new ArrayList<>();

        initDataBase();
    }

    private void initDataBase() {
        this.items.add(new LessonTextItemModel(7, 1, 1, LessonContentType.TEXT, "Pierwszy item w kursie (:", "<p>paragraph</p><h2>h1</h2><h3>h2</h3><h4>h3</h4><ul><li>kropka 1</li><li>kropka 2</li><li>kropka 3</li></ul><ol><li>liczba 1</li><li>liczba 2</li><li>liczba 3</li></ol><figure class=\"table\"><table><thead><tr><th>x</th><th>1</th><th>2</th><th>3</th><th>4</th></tr></thead><tbody><tr><th>A</th><td>A1</td><td>A2</td><td>A3</td><td>A4</td></tr><tr><th>B</th><td>B1</td><td>B2</td><td>B3</td><td>B4</td></tr><tr><th rowspan=\"2\">C</th><td>C1</td><td>C2</td><td>C3</td><td>C4</td></tr><tr><td colspan=\"4\">cccccccccccccccccc</td></tr></tbody></table></figure><p><a href=\"www.google.pl\">LINK</a></p>"));
        this.items.add(new LessonQuestionItemModel(1, 1, 2, LessonContentType.QUESTION, "pytanie1", false));
        this.items.add(new LessonTextItemModel(8, 1, 3, LessonContentType.TEXT, "TYTUł", "<p><b>Peter Piper</b> " +
                "picked a " +
                "peck of pickled peppers; A peck of pickled peppers Peter Piper picked; If Peter Piper picked a peck " +
                "of pickled peppers, Where's the peck of pickled peppers Peter Piper picked?</p>"));
        this.items.add(new LessonQuestionItemModel(3, 1, 4, LessonContentType.QUESTION, "pytanie2", false));
        this.items.add(new LessonTextItemModel(9, 1, 5, LessonContentType.TEXT, "Pierwszy item w kursie (:", "\"<p>fwwaefwawefwafew<strong>ddewfwfeagfegrf</strong><i><strong>wefqfef3q</strong></i><strong>f</strong><i><strong>q3fq3</strong></i></p>"));
        this.items.add(new LessonQuestionItemModel(4, 1, 6, LessonContentType.QUESTION, "pytanie3", false));
        this.items.add(new LessonTextItemModel(10, 1, 7, LessonContentType.TEXT, "Pierwszy item w kursie (:", "<p>paragraph</p><h2>h1</h2><h3>h2</h3><h4>h3</h4><ul><li>kropka 1</li><li>kropka 2</li><li>kropka 3</li></ul><ol><li>liczba 1</li><li>liczba 2</li><li>liczba 3</li></ol><figure class=\"table\"><table><thead><tr><th>x</th><th>1</th><th>2</th><th>3</th><th>4</th></tr></thead><tbody><tr><th>A</th><td>A1</td><td>A2</td><td>A3</td><td>A4</td></tr><tr><th>B</th><td>B1</td><td>B2</td><td>B3</td><td>B4</td></tr><tr><th rowspan=\"2\">C</th><td>C1</td><td>C2</td><td>C3</td><td>C4</td></tr><tr><td colspan=\"4\">cccccccccccccccccc</td></tr></tbody></table></figure><p><a href=\"www.google.pl\">LINK</a></p>"));
        this.items.add(new LessonQuestionItemModel(5, 1, 8, LessonContentType.QUESTION, "pytanie4", true));


        this.items.add(new LessonTextItemModel(11, 2, 1, LessonContentType.TEXT, "Lekcja 2", "<p>paragraph</p><h2>h1</h2><h3>h2</h3><h4>h3</h4><ul><li>kropka 1</li><li>kropka 2</li><li>kropka 3</li></ul><ol><li>liczba 1</li><li>liczba 2</li><li>liczba 3</li></ol><figure class=\"table\"><table><thead><tr><th>x</th><th>1</th><th>2</th><th>3</th><th>4</th></tr></thead><tbody><tr><th>A</th><td>A1</td><td>A2</td><td>A3</td><td>A4</td></tr><tr><th>B</th><td>B1</td><td>B2</td><td>B3</td><td>B4</td></tr><tr><th rowspan=\"2\">C</th><td>C1</td><td>C2</td><td>C3</td><td>C4</td></tr><tr><td colspan=\"4\">cccccccccccccccccc</td></tr></tbody></table></figure><p><a href=\"www.google.pl\">LINK</a></p>"));
        this.items.add(new LessonQuestionItemModel(15, 2, 1, LessonContentType.QUESTION, "pytanie5", false));
        this.items.add(new LessonTextItemModel(12, 2, 2, LessonContentType.TEXT, "Lekcja 2", "<p>paragraph</p><h2>h1</h2><h3>h2</h3><h4>h3</h4><ul><li>kropka 1</li><li>kropka 2</li><li>kropka 3</li></ul><ol><li>liczba 1</li><li>liczba 2</li><li>liczba 3</li></ol><figure class=\"table\"><table><thead><tr><th>x</th><th>1</th><th>2</th><th>3</th><th>4</th></tr></thead><tbody><tr><th>A</th><td>A1</td><td>A2</td><td>A3</td><td>A4</td></tr><tr><th>B</th><td>B1</td><td>B2</td><td>B3</td><td>B4</td></tr><tr><th rowspan=\"2\">C</th><td>C1</td><td>C2</td><td>C3</td><td>C4</td></tr><tr><td colspan=\"4\">cccccccccccccccccc</td></tr></tbody></table></figure><p><a href=\"www.google.pl\">LINK</a></p>"));
        this.items.add(new LessonQuestionItemModel(16, 2, 1, LessonContentType.QUESTION, "pytanie5", false));
        this.items.add(new LessonTextItemModel(13, 2, 3, LessonContentType.TEXT, "Lekcja 2", "<p>paragraph</p><h2>h1</h2><h3>h2</h3><h4>h3</h4><ul><li>kropka 1</li><li>kropka 2</li><li>kropka 3</li></ul><ol><li>liczba 1</li><li>liczba 2</li><li>liczba 3</li></ol><figure class=\"table\"><table><thead><tr><th>x</th><th>1</th><th>2</th><th>3</th><th>4</th></tr></thead><tbody><tr><th>A</th><td>A1</td><td>A2</td><td>A3</td><td>A4</td></tr><tr><th>B</th><td>B1</td><td>B2</td><td>B3</td><td>B4</td></tr><tr><th rowspan=\"2\">C</th><td>C1</td><td>C2</td><td>C3</td><td>C4</td></tr><tr><td colspan=\"4\">cccccccccccccccccc</td></tr></tbody></table></figure><p><a href=\"www.google.pl\">LINK</a></p>"));
        this.items.add(new LessonQuestionItemModel(17, 2, 1, LessonContentType.QUESTION, "pytanie5", false));
        this.items.add(new LessonTextItemModel(14, 2, 4, LessonContentType.TEXT, "Lekcja 23", "<p>paragraph</p><h2>h1</h2><h3>h2</h3><h4>h3</h4><ul><li>kropka 1</li><li>kropka 2</li><li>kropka 3</li></ul><ol><li>liczba 1</li><li>liczba 2</li><li>liczba 3</li></ol><figure class=\"table\"><table><thead><tr><th>x</th><th>1</th><th>2</th><th>3</th><th>4</th></tr></thead><tbody><tr><th>A</th><td>A1</td><td>A2</td><td>A3</td><td>A4</td></tr><tr><th>B</th><td>B1</td><td>B2</td><td>B3</td><td>B4</td></tr><tr><th rowspan=\"2\">C</th><td>C1</td><td>C2</td><td>C3</td><td>C4</td></tr><tr><td colspan=\"4\">cccccccccccccccccc</td></tr></tbody></table></figure><p><a href=\"www.google.pl\">LINK</a></p>"));
        this.items.add(new LessonQuestionItemModel(18, 2, 1, LessonContentType.QUESTION, "pytanie5", true));
    }

    public List<LessonItemTypeModel> getItemsForLesson(int lessonId) {
        return this.items.stream()
                .filter(item -> item.getLessonId() == lessonId)
                .collect(Collectors.toList());
    }

    public Optional<LessonItemTypeModel> addItem(HttpNewLessonItemModel httpNewLessonItemModel) {
        LessonItemTypeModel newItem;
        if (httpNewLessonItemModel.getLessonContentType().equals(LessonContentType.QUESTION)) {
            HttpNewLessonQuestionModel httpNewItem = (HttpNewLessonQuestionModel) httpNewLessonItemModel;
            newItem = LessonQuestionItemModel.of(httpNewItem);
        } else {
            HttpNewLessonTextItemModel httpNewItem = (HttpNewLessonTextItemModel) httpNewLessonItemModel;
            newItem = LessonTextItemModel.of(httpNewItem);
        }

        newItem.setItemId(findFirstFreeId());

        this.items.add(newItem);

        return Optional.of(newItem);
    }

    public Optional<LessonItemTypeModel> addQuestionItem(HttpCreateLessonQuestionItem item, Integer lessonId) {
        LessonItemTypeModel newItem;

        newItem = LessonQuestionItemModel.of(item, lessonId, LessonContentType.QUESTION);

        newItem.setItemId(nextContentId++);
        newItem.setPosition(getNextPosition(lessonId));

        this.items.add(newItem);

        return Optional.of(newItem);
    }

    private Integer getNextPosition(int lessonId) {
        return this.items.stream()
                .filter(content -> content.getLessonId() == lessonId)
                .max(Comparator.comparing(LessonItemTypeModel::getPosition))
                .map(content -> content.getPosition() + 1)
                .orElse(1);
    }

    public Optional<LessonItemTypeModel> editQuestionItem(HttpCreateLessonQuestionItem item) {
        Optional<LessonItemTypeModel> itemOpt = findItemById(item.getId());

        if (itemOpt.isEmpty()) {
            return Optional.empty();
        }

        LessonQuestionItemModel oldItem = (LessonQuestionItemModel) itemOpt.get();

        oldItem.setMulti(item.getMultiAnswer());
        oldItem.setQuestion(item.getQuestion());

        return Optional.of(oldItem);
    }

    public Optional<LessonItemTypeModel> editItem(HttpNewLessonItemModel httpNewLessonItemModel) {
        int id = httpNewLessonItemModel.getItemId();
        Optional<LessonItemTypeModel> optionalItem = findItemById(id);

        if (!optionalItem.isEmpty()) {
            LessonItemTypeModel item;
            if (httpNewLessonItemModel.getLessonContentType().equals(LessonContentType.QUESTION)) {
                HttpNewLessonQuestionModel httpNewItem = (HttpNewLessonQuestionModel) httpNewLessonItemModel;
                item = LessonQuestionItemModel.of(httpNewItem);
            } else {
                HttpNewLessonTextItemModel httpNewItem = (HttpNewLessonTextItemModel) httpNewLessonItemModel;
                item = LessonTextItemModel.of(httpNewItem);
            }

            return Optional.of(item);
        }

        return Optional.empty();

    }

    public Optional<LessonItemTypeModel> findItemById(int id) {
        return this.items.stream()
                .filter(item -> item.getItemId() == id)
                .findFirst();

    }

    private int findFirstFreeId() {
        int size = this.items.size();
        int id = this.items.get(size - 1).getItemId() + 1;
        return id;
    }

    public boolean deleteItem(Integer itemId) {
        Optional<LessonItemTypeModel> optionalLessonItemTypeModel = findItemById(itemId);

        if (!optionalLessonItemTypeModel.isEmpty()) {
            LessonItemTypeModel itemModel = optionalLessonItemTypeModel.get();

            this.items = this.items.stream()
                    .filter(item -> item.getItemId() != itemId)
                    .collect(Collectors.toList());
            return true;
        }

        return false;
    }

    public Optional<LessonItemTypeModel> addTextItem(HttpCreateLessonTextItem item, Integer lessonId) {
        LessonItemTypeModel newItem;

        int id = nextContentId++;
        int position = getNextPosition(lessonId);
        String text = "";
        List<String> texts = item.getText();

        for (String t : texts) {
            text += t;
        }

        newItem = LessonTextItemModel.of(id, item, lessonId, position, LessonContentType.TEXT, text);


        this.items.add(newItem);

        return Optional.of(newItem);
    }

    public Optional<LessonItemTypeModel> editTextItem(HttpCreateLessonTextItem item) {
        Optional<LessonItemTypeModel> itemOpt = findItemById(item.getId());

        if (itemOpt.isEmpty()) {
            return Optional.empty();
        }

        LessonTextItemModel oldItem = (LessonTextItemModel) itemOpt.get();

        String text = "";
        List<String> texts = item.getText();

        for (String t : texts) {
            text += t;
        }

        oldItem.setText(text);
        oldItem.setTitle(item.getTitle());

        return Optional.of(oldItem);
    }
}
