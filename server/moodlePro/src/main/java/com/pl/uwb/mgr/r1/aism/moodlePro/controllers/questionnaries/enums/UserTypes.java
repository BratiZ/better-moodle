package com.pl.uwb.mgr.r1.aism.moodlePro.controllers.questionnaries.enums;

import com.pl.uwb.mgr.r1.aism.moodlePro.database.questionnaires.service.resource.enums.ResourceName;
import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

@Getter
public enum UserTypes {
    NOTYPE(0, "notype"),
    AURAL(1, "sluchowiec"),
    VISUAL(2, "wzrokowiec"),
    KINESTHETIC(3, "kinestetyk");

    int id;
    String name;

    UserTypes(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public static Optional<UserTypes> of(int id) {
        return Arrays.stream(UserTypes.values())
                .filter(typesIds -> id == typesIds.getId())
                .findFirst();
    }
}
