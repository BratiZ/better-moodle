package com.pl.uwb.mgr.r1.aism.moodlePro.controllers.questionnaries.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Data
@Getter
@AllArgsConstructor
public class QuestionnairesAnswersModel {
    private String questionnaireKey;
    private String lng;

    private UserAnswersModel[] answers;
}
