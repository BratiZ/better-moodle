package com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.models;

import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.enums.LessonState;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LessonCompleationModel {
    private int userId;
    private int lessonId;
    private LessonState lessonState;
}
