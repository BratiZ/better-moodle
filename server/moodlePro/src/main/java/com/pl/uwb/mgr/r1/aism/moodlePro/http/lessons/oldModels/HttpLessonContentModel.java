package com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.oldModels;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.enums.LessonContentType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HttpLessonContentModel {
    @JsonProperty("id")
    private int contentId;
    private int position;
    @JsonProperty("item")
    private HttpLessonItemTypeModel lessonItemTypeModel;
    @JsonProperty("type")
    private LessonContentType lessonContentType;
}
