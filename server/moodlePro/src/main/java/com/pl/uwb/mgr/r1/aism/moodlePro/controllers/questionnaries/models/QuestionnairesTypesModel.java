package com.pl.uwb.mgr.r1.aism.moodlePro.controllers.questionnaries.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Data
@AllArgsConstructor
@Getter
public class QuestionnairesTypesModel {
    private Integer id;
    private String name;
    private String key;
}
