package com.pl.uwb.mgr.r1.aism.moodlePro.database.questionnaires.service;

import com.pl.uwb.mgr.r1.aism.moodlePro.database.questionnaires.service.resource.enums.LanguageName;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.questionnaires.service.resource.enums.ResourceName;
import java.io.File;
import java.io.InputStream;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.FileReader;

@Service
public class DataBaseResourcesService {
    private final String RESOURCES_PATH = "data/";

    public FileReader getResources(LanguageName languageName, ResourceName resourceName) throws FileNotFoundException {
        ClassLoader classLoader = this.getClass().getClassLoader();
        String path = buildPath(languageName, resourceName);

        return new FileReader(classLoader.getResource(path).getFile());
    }

    private String buildPath(LanguageName languageName, ResourceName resourceName) {
        return RESOURCES_PATH + languageName.getName() + "/" + "Questionnaire" + resourceName.getName() + ".json";
    }
}
