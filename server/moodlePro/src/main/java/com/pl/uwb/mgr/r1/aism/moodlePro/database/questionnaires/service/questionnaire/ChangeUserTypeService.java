package com.pl.uwb.mgr.r1.aism.moodlePro.database.questionnaires.service.questionnaire;

import com.pl.uwb.mgr.r1.aism.moodlePro.controllers.questionnaries.enums.UserTypes;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.user.models.UserModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ChangeUserTypeService {
    private final UserService userService;

    @Autowired
    public ChangeUserTypeService(UserService userService) {
        this.userService = userService;
    }

    public void changeUserType(String login, int id) {
        Optional<UserTypes> userType = UserTypes.of(id);
        Optional<UserModel> userModel = userService.findUserByLogin(login);
        UserTypes type;
        UserModel user;

        if (!userType.isEmpty() && !userModel.isEmpty()) {
            type = userType.get();
            user = userModel.get();
            user.setUserType(type);
        }
    }
}
