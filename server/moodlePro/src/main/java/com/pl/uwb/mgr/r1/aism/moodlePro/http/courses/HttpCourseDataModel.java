package com.pl.uwb.mgr.r1.aism.moodlePro.http.courses;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.courses.models.CourseModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.user.models.UserModel;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HttpCourseDataModel {
    @JsonProperty("courseId")
    private int id;
    private String name;
    private int subjectId;
    private String subject;
    @JsonProperty("startDate")
    private Date start;
    @JsonProperty("endDate")
    private Date end;
    private int ownerId;
    private boolean isArchived;
    private List<UserModel> students;

    public static HttpCourseDataModel of(CourseModel courseModel) {
        return new HttpCourseDataModel(
                courseModel.getId(),
                courseModel.getName(),
                courseModel.getSubjectId(),
                "",
                courseModel.getStart(),
                courseModel.getEnd(),
                courseModel.getOwnerId(),
                courseModel.isArchived(),
                new ArrayList<>()
        );
    }
}
