package com.pl.uwb.mgr.r1.aism.moodlePro.database.questionnaires.models;

import lombok.Getter;

import java.util.List;

@Getter
public class QuestionnaireModel<T extends QuestionModel> {
    private String name;
    private String language;
    private List<T> questions;
}
