package com.pl.uwb.mgr.r1.aism.moodlePro.utils;

import java.util.Calendar;
import java.util.Date;
import org.springframework.stereotype.Service;

@Service
public class TimeService {

    public Date newDate(int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();

        calendar.set(year, month, day);
        return calendar.getTime();
    }

    public Date today() {
        return Calendar.getInstance().getTime();
    }

    public Date parseDate(String date) {
        Calendar calendar = Calendar.getInstance();
        String[] dateArray = date.split("/");
        int day = Integer.parseInt(dateArray[1]);
        int month = Integer.parseInt(dateArray[0]) - 1;
        int year = Integer.parseInt(dateArray[2]);

        calendar.set(year, month, day);
        return calendar.getTime();
    }
}
