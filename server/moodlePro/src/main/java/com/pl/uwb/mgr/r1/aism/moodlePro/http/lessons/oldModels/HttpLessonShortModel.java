package com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.oldModels;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.models.LessonModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HttpLessonShortModel {
    @JsonProperty("id")
    private int lessonId;
    private String name;

    public static HttpLessonShortModel of (LessonModel lessonModel) {
        return new HttpLessonShortModel(
                lessonModel.getLessonId(),
                lessonModel.getName()
        );
    }
}

