package com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.enums;

public enum LessonState {
    FAIL("TEXT"),
    UNFINISHED("UNFINISHED"),
    PASSED("PASSED");

    private final String typeName;

    LessonState(String typeName) {
        this.typeName = typeName;
    }
}
