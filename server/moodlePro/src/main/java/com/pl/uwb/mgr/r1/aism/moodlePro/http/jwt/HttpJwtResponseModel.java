package com.pl.uwb.mgr.r1.aism.moodlePro.http.jwt;

import com.pl.uwb.mgr.r1.aism.moodlePro.controllers.questionnaries.enums.UserTypes;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class HttpJwtResponseModel {
    private String jwt;
    private String userRole;
    private String refreshToken;
    private UserTypes userType;

//    public HttpJwtResponseModel(String jwt) {
//        this(jwt, "");
//    }
//
//    public HttpJwtResponseModel(String jwt, String userRole) {
//        this(jwt, userRole, "");
//    }

    public HttpJwtResponseModel(String jwt, String userRole, UserTypes userType) {
        this(jwt, userRole, "", userType);
    }
}
