package com.pl.uwb.mgr.r1.aism.moodlePro.http.courses;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HttpEditCourseModel {
    private String name;
    private int subjectId;
    private String startDate;
    private String endDate;
}
