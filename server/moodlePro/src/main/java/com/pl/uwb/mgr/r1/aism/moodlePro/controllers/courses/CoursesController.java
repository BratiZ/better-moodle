package com.pl.uwb.mgr.r1.aism.moodlePro.controllers.courses;

import com.pl.uwb.mgr.r1.aism.moodlePro.core.services.CourseSubjectService;
import com.pl.uwb.mgr.r1.aism.moodlePro.core.services.StudentsCursesService;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.courses.models.CourseModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.courses.services.CoursesService;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.courses.HttpCourseDataModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.courses.HttpEditCourseModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.courses.HttpNewCourseModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/courses")
public class CoursesController {
    private final CoursesService coursesService;
    private final StudentsCursesService studentsCursesService;
    private final CourseSubjectService courseSubjectService;

    @Autowired
    public CoursesController(
            CoursesService coursesService,
            StudentsCursesService studentsCursesService,
            CourseSubjectService courseSubjectService
    ) {
        this.coursesService = coursesService;
        this.studentsCursesService = studentsCursesService;
        this.courseSubjectService = courseSubjectService;
    }

    @GetMapping(
            produces = {
                    MediaType.APPLICATION_JSON_VALUE,
            }
    )
    public ResponseEntity<List<HttpCourseDataModel>> coursesList(Authentication authentication) {
        String login = (String) authentication.getPrincipal();
        List<CourseModel> availableCourses = coursesService.getAvailableCourses(login);

        List<HttpCourseDataModel> httpCourseDataModels = courseSubjectService.connectCoursesWithSubjects(availableCourses);

        return ResponseEntity
                .ok()
                .body(httpCourseDataModels);
    }

    @GetMapping(
            value = "/user"
    )
    public ResponseEntity<List<CourseModel>> userCoursesList(Authentication authentication) {
        String login = (String) authentication.getPrincipal();
        List<CourseModel> courses = studentsCursesService.userCourses(login);

        return ResponseEntity
                .ok()
                .body(courses);
    }

    @GetMapping(
            value = "/{courseId}/data",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity userCourseData(@PathVariable("courseId") Integer courseId) {
        Optional<CourseModel> courseModelOpt = coursesService.findCourseById(courseId);

        return courseModelOpt.isPresent()
                ? ResponseEntity.ok().body(courseModelOpt.get())
                : ResponseEntity.badRequest().body("Cannot find data for course with ID: " + courseId);
    }

    @PutMapping(
            value = "/{id}/delete"
    )
    public ResponseEntity deleteCourse(@PathVariable("id") String courseId) {
        int id = Integer.parseInt(courseId);
        Optional<CourseModel> optionalCourseModel = coursesService.findCourseById(id);

        if (optionalCourseModel.isEmpty()) {
            return ResponseEntity
                    .badRequest()
                    .body("Course do not exist");
        }

        CourseModel course = optionalCourseModel.get();
        coursesService.deleteCourse(course);

        return ResponseEntity
                .ok()
                .build();
    }

    @PutMapping(
            value = "/{id}/stop"
    )
    public ResponseEntity stopCourse(@PathVariable("id") Integer courseId) {
        boolean stopped = coursesService.stopCourse(courseId);

        return stopped
                ? ResponseEntity.ok().build()
                : ResponseEntity.badRequest().body("Cannot find course with Id: " + courseId);
    }

    @PostMapping(
            consumes = {
                    MediaType.APPLICATION_JSON_VALUE,
            }
    )
    public ResponseEntity createCourse(Authentication authentication, @RequestBody HttpNewCourseModel courseModel) throws URISyntaxException {
        String userLogin = (String) authentication.getPrincipal();

        Optional<CourseModel> newCourse = coursesService.createCourse(userLogin, courseModel);

        return newCourse.isPresent()
                ? ResponseEntity.created(new URI("/courses/" + newCourse.get().getId())).build()
                : ResponseEntity.badRequest().body("Cannot create new course");
    }

    @PutMapping(
            value = "/{id}/unarchive"
    )
    public ResponseEntity unarchiveCourse(@PathVariable("id") String courseId) {
        int id = Integer.parseInt(courseId);
        Optional<CourseModel> optionalCourseModel = coursesService.findCourseById(id);

        if (optionalCourseModel.isEmpty()) {
            return ResponseEntity
                    .badRequest()
                    .body("Course with id \"" + courseId + "\" do not exist");
        }

        CourseModel course = optionalCourseModel.get();
        coursesService.unarchiveCourse(course);

        return ResponseEntity
                .ok()
                .build();
    }

    @PutMapping(
            value = "/{id}",
            consumes = {
                    MediaType.APPLICATION_JSON_VALUE,
            }
    )
    public ResponseEntity editCourseName(
            @PathVariable("id") Integer courseId,
            @RequestBody HttpEditCourseModel httpEditCourseNameAndDate
    ) {

        Optional<CourseModel> optionalCourseModel = coursesService.editCourseNameAndDate(
                courseId,
                httpEditCourseNameAndDate
        );

        if (optionalCourseModel.isEmpty()) {
            return ResponseEntity
                    .badRequest()
                    .body("Course with id \"" + courseId + "\" do not exist");
        }

        CourseModel course = optionalCourseModel.get();

        return ResponseEntity
                .ok()
                .body(course);
    }

}
