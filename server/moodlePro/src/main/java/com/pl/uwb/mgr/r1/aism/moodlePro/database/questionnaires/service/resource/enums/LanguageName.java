package com.pl.uwb.mgr.r1.aism.moodlePro.database.questionnaires.service.resource.enums;

import lombok.Getter;

import java.util.Arrays;

@Getter
public enum LanguageName {
    PL("pl"),
    EN("en"),
    RU("ru");
    String name;

    LanguageName(String name) {
        this.name = name;
    }

    public static LanguageName of(String name) {
        return Arrays.stream(LanguageName.values())
                .filter(languageName -> name.toUpperCase().equals(languageName.getName().toUpperCase()))
                .findFirst()
                .orElse(EN);
    }
}
