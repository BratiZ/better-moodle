package com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.newModels;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class HttpCreateLessonAnswerItem {
    private Integer id;
    private String answer;
    private Boolean isCorrect;
}
