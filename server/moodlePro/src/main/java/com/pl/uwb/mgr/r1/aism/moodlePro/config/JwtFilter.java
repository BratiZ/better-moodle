package com.pl.uwb.mgr.r1.aism.moodlePro.config;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.security.SignatureException;
import java.io.IOException;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Slf4j
public class JwtFilter extends BasicAuthenticationFilter {
    public static final String KEY_JWT = "(~[L:lM47I]8-RSp[CQH?&p.fb[c_B+Wd]Z#uz**7WQ{a[yf>YKA9SuVfwGe>;;";

    public JwtFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        String header = request.getHeader("Authorization");
        Optional<UsernamePasswordAuthenticationToken> authenticationByTokenOpt = getAuthenticationByToken(
                header,
                request,
                response);

        if (authenticationByTokenOpt.isPresent()) {
            UsernamePasswordAuthenticationToken authResult = authenticationByTokenOpt.get();
            SecurityContextHolder.getContext().setAuthentication(authResult);
        }

        chain.doFilter(request, response);
    }

    private Optional<UsernamePasswordAuthenticationToken> getAuthenticationByToken(
            String header,
            HttpServletRequest httpServletRequest,
            HttpServletResponse response) throws IOException {
        if (StringUtils.isBlank(header)) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Empty header: Authorization");

            return Optional.empty();
        }

        String token = header.replace("Bearer ", "");

        Optional<Jws<Claims>> claimsJwsOpt = validateToken(token, httpServletRequest);

        if (claimsJwsOpt.isEmpty()) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid JWT token");

            return Optional.empty();
        }

        Jws<Claims> claimsJws = claimsJwsOpt.get();

        String username = claimsJws.getBody().get("login").toString();
        String role = claimsJws.getBody().get("role").toString();

        Set<SimpleGrantedAuthority> simpleGrantedAuthorities = Collections.singleton(new SimpleGrantedAuthority(role));

        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                username,
                null,
                simpleGrantedAuthorities);

        return Optional.of(usernamePasswordAuthenticationToken);
    }

    private Optional<Jws<Claims>> validateToken(String token, HttpServletRequest httpServletRequest) {
        try {
            Jws<Claims> claimsJws = Jwts.parser()
                    .setSigningKey(KEY_JWT.getBytes())
                    .parseClaimsJws(token);

            return Optional.of(claimsJws);
        } catch (SignatureException ex) {
            log.error("Invalid JWT Signature");
        } catch (MalformedJwtException ex) {
            log.error("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            log.error("Expired JWT token");
            httpServletRequest.setAttribute("expired", ex.getMessage());
        } catch (UnsupportedJwtException ex) {
            log.error("Unsupported JWT exception");
        } catch (IllegalArgumentException ex) {
            log.error("Jwt claims string is empty");
        }

        return Optional.empty();
    }
}
