package com.pl.uwb.mgr.r1.aism.moodlePro.database.questionnaires.models;

import lombok.Getter;

@Getter
public class QuestionCloseModel extends QuestionModel {
    CloseTypeDataModel closeTypeData;
}
