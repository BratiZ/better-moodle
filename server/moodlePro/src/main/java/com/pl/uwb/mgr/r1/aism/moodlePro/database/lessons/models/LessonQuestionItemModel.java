package com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.models;

import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.enums.LessonContentType;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.newModels.HttpCreateLessonQuestionItem;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.newModels.HttpNewLessonQuestionModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LessonQuestionItemModel extends LessonItemTypeModel {
    private String question;
    private boolean isMulti;

    public LessonQuestionItemModel(int contentId,
                                   int lessonId,
                                   int position,
                                   LessonContentType lessonContentType,
                                   String question,
                                   boolean isMulti) {
        super(contentId, lessonId, position, lessonContentType);
        this.question = question;
        this.isMulti = isMulti;
    }

    public static LessonQuestionItemModel of(HttpNewLessonQuestionModel model) {
        return new LessonQuestionItemModel(
                model.getItemId(),
                model.getLessonId(),
                model.getPosition(),
                model.getLessonContentType(),
                model.getQuestion(),
                model.isMulti()
        );

    }

    public static LessonQuestionItemModel of(HttpCreateLessonQuestionItem model, Integer lessonId,
                                             LessonContentType type) {
        return new LessonQuestionItemModel(
                model.getId(),
                lessonId,
                1,
                type,
                model.getQuestion(),
                model.getMultiAnswer()
        );
    }
}
