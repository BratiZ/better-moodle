package com.pl.uwb.mgr.r1.aism.moodlePro.database.user.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TeacherModel {
    private int teacherId;
    private String name;
    private String email;

    public static TeacherModel of(UserModel userModel) {
        return new TeacherModel(
                userModel.getId(),
                userModel.getLogin(),
                userModel.getEmail()
        );
    }
}
