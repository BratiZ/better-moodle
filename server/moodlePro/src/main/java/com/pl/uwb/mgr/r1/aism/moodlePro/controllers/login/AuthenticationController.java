package com.pl.uwb.mgr.r1.aism.moodlePro.controllers.login;

import com.pl.uwb.mgr.r1.aism.moodlePro.config.JwtFilter;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.jwt.HttpJwtResponseModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.user.models.UserModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.user.service.UserService;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.user.HttpUserLoginModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.user.HttpUserRegistrationModel;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.util.Date;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class AuthenticationController {
    private final UserService userService;

    @Autowired
    public AuthenticationController(UserService userService) {

        this.userService = userService;
    }

    @PostMapping(
            value = "/login",
            consumes = {
                    MediaType.APPLICATION_JSON_VALUE,
                    MediaType.APPLICATION_XML_VALUE
            }
    )
    public ResponseEntity login(@RequestBody HttpUserLoginModel userHttpModel) {
        long time = System.currentTimeMillis();

        Optional<UserModel> userModelOptional = userService.userLogin(userHttpModel);

        if (userModelOptional.isEmpty()) {
            return ResponseEntity
                    .badRequest()
                    .body("Login Error - Incorrect login or password");
        }

        UserModel userModel = userModelOptional.get();

        String token = Jwts.builder()
                .setSubject("s_" + userModel.getLogin())
                .claim("login", userModel.getLogin())
                .claim("role", "ROLE_" + userModel.getRole())
                .setIssuedAt(new Date(time))
//                .setExpiration(new Date(time + 15 * 60_000)) // todo enable time expiration
                .signWith(SignatureAlgorithm.HS256, JwtFilter.KEY_JWT.getBytes())
                .compact();

        return ResponseEntity
                .ok()
                .body(
                        new HttpJwtResponseModel(
                                token,
                                userModel.getRole(),
                                userModel.getUserType()
                        )
                );
    }

    @PostMapping(
            value = "/register",
            consumes = {
                    MediaType.APPLICATION_JSON_VALUE,
                    MediaType.APPLICATION_XML_VALUE
            }
    )
    public ResponseEntity register(@RequestBody HttpUserRegistrationModel httpUserRegistrationModel) {

        Optional<UserModel> userModelOptional = userService.userRegistration(httpUserRegistrationModel);

        if (userModelOptional.isEmpty()) {
            return ResponseEntity
                    .badRequest()
                    .body("Registration Error - Incorrect login or mail");
        }

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body("");
    }


}
