package com.pl.uwb.mgr.r1.aism.moodlePro.controllers.questionnaries;

import com.pl.uwb.mgr.r1.aism.moodlePro.controllers.questionnaries.enums.UserTypes;
import com.pl.uwb.mgr.r1.aism.moodlePro.controllers.questionnaries.models.QuestionnairesAnswersModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.controllers.questionnaries.models.QuestionnairesTypesModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.core.jsonparser.JsonParserService;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.questionnaires.models.QuestionCloseModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.questionnaires.models.QuestionnaireModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.questionnaires.service.questionnaire.ChangeUserTypeService;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.questionnaires.service.questionnaire.QuestionnairesResultsService;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.questionnaires.service.resource.enums.LanguageName;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.questionnaires.service.resource.enums.ResourceName;
import java.io.FileNotFoundException;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/{lng}/questionnaire")
public class QuestionnairesController {
    private final JsonParserService jsonParserService;
    private final QuestionnairesResultsService questionnaireResultsService;
    private final ChangeUserTypeService changeUserTypeService;
    @Autowired
    public QuestionnairesController(JsonParserService jsonParserService,
                                    QuestionnairesResultsService questionnaireResultsService,
                                    ChangeUserTypeService changeUserTypeService) {
        this.jsonParserService = jsonParserService;
        this.questionnaireResultsService = questionnaireResultsService;
        this.changeUserTypeService = changeUserTypeService;
    }

    @ResponseBody
    @PostMapping(
            value = "/results",
            consumes = {
                    MediaType.APPLICATION_JSON_VALUE,
                    MediaType.APPLICATION_XML_VALUE
            },
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity results(Authentication authentication, @PathVariable("lng") String lng, @RequestBody QuestionnairesAnswersModel questionnairesAnswersModel) {
        QuestionnairesTypesModel questionnairesTypesModel = questionnaireResultsService.userType(questionnairesAnswersModel);

        String login = (String)authentication.getPrincipal();
        int id = questionnairesTypesModel.getId();

        changeUserTypeService.changeUserType(login, id);

        return ResponseEntity.ok(questionnairesTypesModel);
    }

    @ResponseBody
    @GetMapping(value = "/{key}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity TEST_findById(@PathVariable("lng") String lng, @PathVariable("key") String key) throws FileNotFoundException {


        Optional<ResourceName> resourceName = ResourceName.of(key);

        if (resourceName.isEmpty()) {
            return ResponseEntity.
                    status(HttpStatus.BAD_REQUEST)
                    .body("ŹLE");
        }

        QuestionnaireModel<QuestionCloseModel> questionnaireModel = jsonParserService.parse(
                LanguageName.of(lng),
                resourceName.get()
        );

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(questionnaireModel);
    }
}
