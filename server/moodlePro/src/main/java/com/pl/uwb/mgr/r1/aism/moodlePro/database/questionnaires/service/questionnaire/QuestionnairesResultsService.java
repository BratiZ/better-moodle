package com.pl.uwb.mgr.r1.aism.moodlePro.database.questionnaires.service.questionnaire;


import com.pl.uwb.mgr.r1.aism.moodlePro.controllers.questionnaries.enums.UserTypes;
import com.pl.uwb.mgr.r1.aism.moodlePro.controllers.questionnaries.models.QuestionnairesAnswersModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.controllers.questionnaries.models.QuestionnairesTypesModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.controllers.questionnaries.models.UserAnswersModel;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class QuestionnairesResultsService {

    public QuestionnairesTypesModel userType(QuestionnairesAnswersModel questionnairesAnswersModel) {
        boolean isAnswerEmpty = false;
        QuestionnairesTypesModel questionnairesTypesModel;
        UserAnswersModel[] userAnswersModel = questionnairesAnswersModel.getAnswers();
        String[] answers;
        int[] answersType = new int[3];
        for (UserAnswersModel userAnswerModel : userAnswersModel) {
            answers = userAnswerModel.getAnswers();
            for (String answer : answers) {
                switch (answer) {
                    case "a":
                        answersType[0]++;
                        break;
                    case "b":
                        answersType[1]++;
                        break;
                    case "c":
                        answersType[2]++;
                        break;
                }
            }
        }


        if (answersType[0] > answersType[1] && answersType[0] > answersType[2]) {
            questionnairesTypesModel = new QuestionnairesTypesModel(UserTypes.VISUAL.getId(), UserTypes.VISUAL.getName(), UserTypes.VISUAL.toString());
        } else if (answersType[1] > answersType[0] && answersType[1] > answersType[2]) {
            questionnairesTypesModel = new QuestionnairesTypesModel(UserTypes.AURAL.getId(), UserTypes.AURAL.getName(), UserTypes.AURAL.toString());
        } else {
            questionnairesTypesModel = new QuestionnairesTypesModel(UserTypes.KINESTHETIC.getId(), UserTypes.KINESTHETIC.getName(), UserTypes.KINESTHETIC.toString());
        }

        return questionnairesTypesModel;
    }
}
