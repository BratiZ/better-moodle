package com.pl.uwb.mgr.r1.aism.moodlePro.core.jsonparser;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.questionnaires.models.QuestionCloseModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.questionnaires.models.QuestionnaireModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.questionnaires.service.DataBaseResourcesService;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.questionnaires.service.resource.enums.LanguageName;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.questionnaires.service.resource.enums.ResourceName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Type;

@Service
public class JsonParserService {

    private Gson gson = new Gson();
    private JsonReader jsonReader;
    private final DataBaseResourcesService dataBaseResourcesService;

    @Autowired
    public JsonParserService(DataBaseResourcesService dataBaseResourcesService) {
        this.dataBaseResourcesService = dataBaseResourcesService;
    }

    public QuestionnaireModel parse(LanguageName languageName, ResourceName resourceName) throws FileNotFoundException {
        FileReader fileReader = dataBaseResourcesService.getResources(languageName, resourceName);
        jsonReader = new JsonReader(fileReader);
        Type collectionType = new TypeToken<QuestionnaireModel<QuestionCloseModel>>() {}.getType();
        QuestionnaireModel<QuestionCloseModel> questionnaireModel = gson.fromJson(jsonReader, collectionType);

        return questionnaireModel;
    }


}
