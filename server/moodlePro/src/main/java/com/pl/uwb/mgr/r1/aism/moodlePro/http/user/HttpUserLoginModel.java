package com.pl.uwb.mgr.r1.aism.moodlePro.http.user;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class HttpUserLoginModel {
    private String login;
    private String password;
}
