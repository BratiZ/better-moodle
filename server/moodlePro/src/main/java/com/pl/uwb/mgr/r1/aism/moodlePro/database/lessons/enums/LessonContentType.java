package com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.enums;

import lombok.Getter;

@Getter
public enum LessonContentType {
    TEXT("TEXT"),
    QUESTION("QUESTION");

    private final String typeName;

    LessonContentType(String typeName) {
        this.typeName = typeName;
    }
}
