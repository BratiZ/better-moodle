package com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.models;

import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.enums.LessonContentType;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.newModels.HttpCreateLessonTextItem;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.newModels.HttpNewLessonTextItemModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LessonTextItemModel extends LessonItemTypeModel {
    private String title;
    private String text;

    public LessonTextItemModel(
            int contentId,
            int lessonId,
            int position,
            LessonContentType lessonContentType,
            String title,
            String text
    ) {
        super(contentId, lessonId, position, lessonContentType);
        this.title = title;
        this.text = text;
    }

    public static LessonTextItemModel of(HttpNewLessonTextItemModel model) {
        return new LessonTextItemModel(
                model.getItemId(),
                model.getLessonId(),
                model.getPosition(),
                model.getLessonContentType(),
                model.getTitle(),
                model.getText()
        );
    }

    public static LessonTextItemModel of(
            int id,
            HttpCreateLessonTextItem model,
            int lessonId,
            int position,
            LessonContentType lessonContentType,
            String text
    ) {
        return new LessonTextItemModel(
                id,
                lessonId,
                position,
                lessonContentType,
                model.getTitle(),
                text
        );
    }
}
