package com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.newModels;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HttpCreateLessonTextItem implements HttpCreateLessonItem{
    private int id;
    @JsonProperty("content")
    private List<String> text;
    private String title;
}
