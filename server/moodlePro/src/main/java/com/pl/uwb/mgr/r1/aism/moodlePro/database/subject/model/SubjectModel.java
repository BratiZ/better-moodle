package com.pl.uwb.mgr.r1.aism.moodlePro.database.subject.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SubjectModel {
    @JsonProperty("subjectId")
    private int id;
    @JsonProperty("subjectName")
    private String name;
    private boolean isArchived;
}
