package com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.newModels.HttpCreateLessonAnswerItem;
import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LessonQuestionAnswerModel {
    @JsonProperty("id")
    private int answerId;
    private int questionId;
    private String answer;
    @JsonProperty("isCorrect")
    private boolean isCorrect;

    public static LessonQuestionAnswerModel of(HttpCreateLessonAnswerItem item, int id, int questionId) {
        return new LessonQuestionAnswerModel(
                id,
                questionId,
                item.getAnswer(),
                item.getIsCorrect()
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LessonQuestionAnswerModel that = (LessonQuestionAnswerModel) o;
        return answerId == that.answerId;
    }
}
