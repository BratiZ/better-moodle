package com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.newModels;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.enums.LessonContentType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract class HttpNewLessonItemModel {
    @JsonProperty("id")
    private int itemId;
    private int lessonId;
    private int position;
    private LessonContentType lessonContentType;
}
