package com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.newModels;

import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.enums.LessonState;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HttpLessonCompleationModel {
    private int lessonId;
    private LessonState lessonState;
    private int userPoints;
    private int maxPoints;
}
