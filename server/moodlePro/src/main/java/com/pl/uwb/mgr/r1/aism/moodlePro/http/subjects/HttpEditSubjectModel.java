package com.pl.uwb.mgr.r1.aism.moodlePro.http.subjects;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HttpEditSubjectModel {
    private int id;
    private String name;
}
