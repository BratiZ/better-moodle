package com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.oldModels;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.enums.LessonContentType;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.models.LessonQuestionAnswerModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.models.LessonQuestionItemModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HttpLessonQuestionModel extends HttpLessonItemTypeModel {
    private String question;
    @JsonProperty("multiAnswer")
    private boolean isMulti;
    private List<LessonQuestionAnswerModel> answers;

    public HttpLessonQuestionModel(
            int contentId,
            int lessonId,
            int position,
            LessonContentType lessonContentType,
            String question,
            boolean isMulti,
            List<LessonQuestionAnswerModel> answers) {
        super(contentId, lessonId, position, lessonContentType);
        this.question = question;
        this.isMulti = isMulti;
        this.answers = answers;
    }

    public static HttpLessonQuestionModel of(LessonQuestionItemModel lessonQuestionItemModel, List<LessonQuestionAnswerModel> answers) {
        return new HttpLessonQuestionModel(
                lessonQuestionItemModel.getItemId(),
                lessonQuestionItemModel.getLessonId(),
                lessonQuestionItemModel.getPosition(),
                lessonQuestionItemModel.getLessonContentType(),
                lessonQuestionItemModel.getQuestion(),
                lessonQuestionItemModel.isMulti(),
                answers
        );
    }
}
