package com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.newModels;

import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.enums.LessonContentType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HttpNewLessonQuestionModel extends HttpNewLessonItemModel{
    private String question;
    private boolean isMulti;

    public HttpNewLessonQuestionModel(int contentId,
                                   int lessonId,
                                   int position,
                                   LessonContentType lessonContentType,
                                   String question,
                                   boolean isMulti) {
        super(contentId, lessonId, position, lessonContentType);
        this.question = question;
        this.isMulti = isMulti;
    }
}
