package com.pl.uwb.mgr.r1.aism.moodlePro.core.services;

import com.pl.uwb.mgr.r1.aism.moodlePro.database.user.enums.UserRole;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.user.models.UserModel;
import org.springframework.stereotype.Service;

@Service
public class PermissionsService {
    public boolean hasUserPermission(UserRole role, UserModel userModel) {
        String userRole = userModel.getRole();

        return role.getKey().equalsIgnoreCase(userRole);
    }
}
