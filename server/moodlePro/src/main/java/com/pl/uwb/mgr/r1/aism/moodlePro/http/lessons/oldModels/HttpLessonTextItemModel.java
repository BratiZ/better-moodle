package com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.oldModels;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.enums.LessonContentType;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.models.LessonTextItemModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HttpLessonTextItemModel extends HttpLessonItemTypeModel {
    private String title;
    @JsonProperty("content")
    private List<String> text;

    public HttpLessonTextItemModel(
            int itemId,
            int lessonId,
            int position,
            LessonContentType lessonContentType,
            String title,
            List<String> text) {
        super(itemId, lessonId, position, lessonContentType);
        this.title = title;
        this.text = text;
    }

    public static HttpLessonItemTypeModel of(LessonTextItemModel lessonItemTypeModel) {
        List<String> text = new ArrayList<>();
        text.add(lessonItemTypeModel.getText());
        return new HttpLessonTextItemModel(
                lessonItemTypeModel.getItemId(),
                lessonItemTypeModel.getLessonId(),
                lessonItemTypeModel.getPosition(),
                lessonItemTypeModel.getLessonContentType(),
                lessonItemTypeModel.getTitle(),
                text
        );

    }
}
