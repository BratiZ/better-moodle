package com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.newModels;

import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.enums.LessonContentType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HttpNewLessonTextItemModel extends HttpNewLessonItemModel {
    private String title;
    private String text;

    public HttpNewLessonTextItemModel(
            int contentId,
            int lessonId,
            int position,
            LessonContentType lessonContentType,
            String title,
            String text) {
        super(contentId, lessonId, position, lessonContentType);
        this.title = title;
        this.text = text;
    }
}

