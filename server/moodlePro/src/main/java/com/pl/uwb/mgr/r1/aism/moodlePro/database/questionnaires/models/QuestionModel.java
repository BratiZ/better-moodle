package com.pl.uwb.mgr.r1.aism.moodlePro.database.questionnaires.models;

import lombok.Getter;

@Getter
public abstract class QuestionModel {
    String type;
    String nr;
    String description;

}
