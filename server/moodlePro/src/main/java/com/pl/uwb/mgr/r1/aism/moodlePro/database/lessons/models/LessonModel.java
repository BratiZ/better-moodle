package com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.models;

import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.enums.LessonContentType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LessonModel {
    
    private int lessonId;
    private int courseId;
    private String name;
}
