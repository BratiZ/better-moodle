package com.pl.uwb.mgr.r1.aism.moodlePro.database.user.service;

import com.pl.uwb.mgr.r1.aism.moodlePro.controllers.questionnaries.enums.UserTypes;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.user.enums.UserRole;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.user.models.UserModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.user.HttpUserLoginModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.user.HttpUserRegistrationModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.Getter;
import org.springframework.stereotype.Service;

@Getter
@Service
public class UserService {
    private List<UserModel> users;

    public UserService() {
        this.users = new ArrayList<>();

        this.users.add(new UserModel(1, "admin", "admin", UserRole.ADMIN.getKey(), "admin@btr.mdl", UserTypes.NOTYPE));
        this.users.add(new UserModel(2, "user", "user", UserRole.USER.getKey(), "user@btr.mdl", UserTypes.VISUAL));
        this.users.add(new UserModel(3, "teacher", "teacher", UserRole.TEACHER.getKey(), "user2@btr.mdl", UserTypes.NOTYPE));
        this.users.add(new UserModel(4, "user2", "user2", UserRole.USER.getKey(), "user3@btr.mdl", UserTypes.AURAL));
        this.users.add(new UserModel(5, "user3", "user3", UserRole.USER.getKey(), "user4@btr.mdl",
                UserTypes.KINESTHETIC));
    }

    public Optional<UserModel> userLogin(HttpUserLoginModel httpUserLoginModel) {
        return users.stream()
                .filter(user -> user.getLogin().equals(httpUserLoginModel.getLogin()))
                .filter(user -> user.getPassword().equals(httpUserLoginModel.getPassword()))
                .findFirst();
    }

    public Optional<UserModel> userRegistration(HttpUserRegistrationModel httpUserRegistrationModel) {

        if (this.userNameExist(httpUserRegistrationModel.getLogin())) {
            return Optional.empty();
        }

        if (this.userMailExist(httpUserRegistrationModel.getLogin())) {
            return Optional.empty();
        }

        users.add(UserModel.of(httpUserRegistrationModel));

        return findUserByLogin(httpUserRegistrationModel.getLogin());
    }

    public Optional<UserModel> findUserByLogin(String login) {
        return users.stream()
                .filter(user -> user.getLogin().equals(login))
                .findFirst();
    }

    public Optional<UserModel> findUserById(int id) {
        return users.stream()
                .filter(user -> user.getId() == id)
                .findFirst();
    }

    private boolean userNameExist(String userName) {
        return users.stream().map(UserModel::getLogin).anyMatch(userName::equals);
    }

    private boolean userMailExist(String userName) {
        return users.stream().map(UserModel::getEmail).anyMatch(userName::equals);
    }

    public Optional<UserModel> editUserRole(Integer userId, String newRole) {
        Optional<UserModel> optionalUserModel = findUserById(userId);
        Optional<UserRole> optionalUserRole = UserRole.of(newRole);

        if (optionalUserModel.isPresent() && optionalUserRole.isPresent()) {
            UserModel user = optionalUserModel.get();
            UserRole role = optionalUserRole.get();

            user.setRole(role.getKey());

            return Optional.of(user);
        }

        return Optional.empty();
    }
}
