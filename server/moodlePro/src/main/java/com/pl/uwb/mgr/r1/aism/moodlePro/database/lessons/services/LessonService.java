package com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.services;

import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.enums.LessonContentType;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.models.LessonItemTypeModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.models.LessonModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.models.LessonQuestionAnswerModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.models.LessonQuestionItemModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.models.LessonTextItemModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.oldModels.HttpLessonContentModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.oldModels.HttpLessonQuestionModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.oldModels.HttpLessonShortModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.oldModels.HttpLessonTextItemModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LessonService {
    private List<LessonModel> lessons;
    private int firstFreeId;

    private final LessonItemService lessonItemService;
    private final LessonQuestionAnswerService lessonQuestionAnswerService;

    @Autowired
    public LessonService(
            LessonItemService lessonItemService,
            LessonQuestionAnswerService lessonQuestionAnswerService
    ) {
        this.lessons = new ArrayList<>();

        this.lessonItemService = lessonItemService;
        this.lessonQuestionAnswerService = lessonQuestionAnswerService;

        initDataBase();
    }

    private void initDataBase() {
        this.lessons.add(new LessonModel(1, 1, "Wprowadzenie"));
        this.lessons.add(new LessonModel(2, 1, "Ciąg fibbonaciego"));
        this.lessons.add(new LessonModel(3, 2, "Wprowadzenie"));
        this.lessons.add(new LessonModel(4, 2, "Złożoność wykładnicza"));

        this.firstFreeId = 5;
    }

    public List<HttpLessonShortModel> getLessonsForCourse(int courseId) {
        return this.lessons.stream()
                .filter(lesson -> lesson.getCourseId() == courseId)
                .map(HttpLessonShortModel::of)
                .collect(Collectors.toList());
    }

    public List<LessonModel> getLessonsModelsForCourse(int courseId) {
        return this.lessons.stream()
                .filter(lesson -> lesson.getCourseId() == courseId)
                .collect(Collectors.toList());
    }

    public List<HttpLessonContentModel> getLessonContent(int lessonId) {
        List<HttpLessonContentModel> content = new ArrayList<>();
        List<LessonItemTypeModel> items = lessonItemService.getItemsForLesson(lessonId);
        List<LessonQuestionAnswerModel> answers;

        if (!items.isEmpty()) {
            for (LessonItemTypeModel item : items) {
                if (item.getLessonContentType().equals(LessonContentType.QUESTION)) {
                    LessonQuestionItemModel lessonQuestionItemModel = (LessonQuestionItemModel) item;
                    content.add(new HttpLessonContentModel(item.getItemId(),
                            item.getPosition(),
                            HttpLessonQuestionModel.of(lessonQuestionItemModel, lessonQuestionAnswerService.getAnswersForQuestion(item.getItemId())),
                            item.getLessonContentType()
                    ));
                } else {
                    LessonTextItemModel lessonTextItemModel = (LessonTextItemModel) item;
                    content.add(new HttpLessonContentModel(item.getItemId(),
                            item.getPosition(),
                            HttpLessonTextItemModel.of(lessonTextItemModel),
                            item.getLessonContentType()));
                }
            }
        }

        return content;
    }

    public boolean deleteLesson(Integer lessonId) {
        Optional<LessonModel> optionalLessonModel = findLessonById(lessonId);

        if (!optionalLessonModel.isEmpty()) {
            LessonModel lessonModel = optionalLessonModel.get();

            this.lessons = this.lessons.stream()
                    .filter(lesson -> lesson.getLessonId() != lessonId)
                    .collect(Collectors.toList());
            return true;
        }

        return false;
    }

    private Optional<LessonModel> findLessonById(Integer lessonId) {
        return this.lessons.stream()
                .filter(lesson -> lesson.getLessonId() == lessonId)
                .findFirst();
    }

    public Optional<LessonModel> editLessonName(Integer lessonId, String newName) {
        Optional<LessonModel> lessonModelOpt = findLessonById(lessonId);

        if (lessonModelOpt.isEmpty()) {
            return Optional.empty();
        }

        LessonModel lessonModel = lessonModelOpt.get();
        lessonModel.setName(newName);

        return Optional.of(lessonModel);
    }

    public Optional<LessonModel> createLesson(Integer courseId, String name) {
        int id = firstFreeId;
        firstFreeId++;

        LessonModel newModel = new LessonModel(id, courseId, name);
        this.lessons.add(newModel);

        return Optional.of(newModel);
    }
}
