package com.pl.uwb.mgr.r1.aism.moodlePro.http.courses;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class HttpNewCourseModel {
    private String courseName;
    private int subjectId;
    private String startDate;
    private String endDate;
}
