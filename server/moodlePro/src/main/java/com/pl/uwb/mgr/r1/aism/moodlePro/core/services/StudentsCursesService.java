package com.pl.uwb.mgr.r1.aism.moodlePro.core.services;

import com.pl.uwb.mgr.r1.aism.moodlePro.database.courses.models.CourseModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.courses.services.CoursesService;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.user.models.UserModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class StudentsCursesService {
    private Map<Integer, List<Integer>> coursesStudentsList;
    private UserService userService;
    private CoursesService coursesService;

    @Autowired
    public StudentsCursesService(
            UserService userService,
            CoursesService coursesService
    ) {
        this.coursesStudentsList = new HashMap<>();
        this.userService = userService;
        this.coursesService = coursesService;

        this.coursesStudentsList.put(1, new ArrayList<>());
        this.coursesStudentsList.put(2, new ArrayList<>());

        this.coursesStudentsList.get(1).add(1);
        this.coursesStudentsList.get(1).add(2);

        this.coursesStudentsList.get(2).add(1);
    }

    public List<CourseModel> userCourses(String login) {
        List<CourseModel> courses = new ArrayList<>();
        Optional<UserModel> userModel = userService.findUserByLogin(login);
        UserModel user;
        Optional<CourseModel> courseModel;
        CourseModel course;
        Optional<Integer> userId;
        if (!userModel.isEmpty()) {
            user = userModel.get();
           for (Map.Entry<Integer, List<Integer>> entry : coursesStudentsList.entrySet()) {
               userId =entry.getValue().stream()
                            .filter(id -> id == user.getId())
                    .findFirst();
               if (!userId.isEmpty()) {
                   courseModel = coursesService.findCourseById(entry.getKey());
                   if (!courseModel.isEmpty()) {
                       course = courseModel.get();
                       courses.add(course);
                   }
               }
           }
        }
        return courses;
    }
}
