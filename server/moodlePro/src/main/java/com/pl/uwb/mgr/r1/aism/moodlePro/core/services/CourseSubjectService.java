package com.pl.uwb.mgr.r1.aism.moodlePro.core.services;

import com.pl.uwb.mgr.r1.aism.moodlePro.database.courses.models.CourseModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.subject.services.SubjectsService;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.subject.model.SubjectModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.courses.HttpCourseDataModel;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class CourseSubjectService {
    public final static String SUBJECT_NOT_FOUND = "NO_SUBJECT_IN_DB";
    private final SubjectsService subjectsService;

    public CourseSubjectService(
            SubjectsService subjectsService
    ) {
        this.subjectsService = subjectsService;
    }

    public List<HttpCourseDataModel> connectCoursesWithSubjects(List<CourseModel> courseModels) {
        return courseModels.stream()
                .map(HttpCourseDataModel::of)
                .peek(course -> course.setSubject(getSubjectName(course.getSubjectId())))
                .collect(Collectors.toList());
    }

    private String getSubjectName(int subjectId) {
        Optional<SubjectModel> subjectById = subjectsService.findSubjectById(subjectId);

        return subjectById.isPresent()
                ? subjectById.get().getName()
                : SUBJECT_NOT_FOUND;
    }
}
