package com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.newModels;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HttpLessonPointsCountModel {
    private int questionId;
    private List<Integer> answers;
}
