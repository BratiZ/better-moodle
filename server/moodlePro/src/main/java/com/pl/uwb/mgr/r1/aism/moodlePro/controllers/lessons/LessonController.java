package com.pl.uwb.mgr.r1.aism.moodlePro.controllers.lessons;

import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.enums.LessonState;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.models.LessonItemTypeModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.models.LessonModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.services.LessonCompleationService;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.services.LessonItemService;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.services.LessonQuestionAnswerService;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.services.LessonService;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.newModels.HttpCreateLessonQuestionItem;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.newModels.HttpCreateLessonTextItem;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.newModels.HttpLessonCompleationModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.newModels.HttpLessonPointsCountModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.oldModels.HttpCourseLessonModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.oldModels.HttpLessonContentModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.oldModels.HttpLessonShortModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/courses/{courseId}/lessons")
public class LessonController {
    private final LessonService lessonService;
    private final LessonItemService lessonItemService;
    private final LessonQuestionAnswerService lessonQuestionAnswerService;
    private final LessonCompleationService lessonCompleationService;

    @Autowired
    public LessonController(
            LessonService lessonService,
            LessonItemService lessonItemService,
            LessonQuestionAnswerService lessonQuestionAnswerService,
            LessonCompleationService lessonCompleationService
    ) {

        this.lessonService = lessonService;
        this.lessonItemService = lessonItemService;
        this.lessonQuestionAnswerService = lessonQuestionAnswerService;
        this.lessonCompleationService = lessonCompleationService;
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<HttpLessonShortModel>> coursesList(
            @PathVariable("courseId") Integer courseId
    ) {

        List<HttpLessonShortModel> lessons = lessonService.getLessonsForCourse(courseId);

        if (lessons.isEmpty()) {
            return ResponseEntity
                    .ok()
                    .body(new ArrayList<>());
        }


        return ResponseEntity
                .ok()
                .body(lessons);
    }

    @GetMapping(
            value = "/{lessonId}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<HttpLessonContentModel>> getLessonContent(
            @PathVariable("lessonId") Integer lessonId
    ) {

        List<HttpLessonContentModel> content = lessonService.getLessonContent(lessonId);

        if (content.isEmpty()) {
            return ResponseEntity
                    .ok()
                    .body(new ArrayList<>());
        }

        return ResponseEntity
                .ok()
                .body(content);
    }


    @PostMapping(
            value = "/{lessonId}/question",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity editLessonContentQuestionItem(
            @PathVariable("lessonId") Integer lessonId,
            @RequestBody HttpCreateLessonQuestionItem item
    ) {

        if (item.getId() < 0) {
            Optional<LessonItemTypeModel> newItem = lessonItemService.addQuestionItem(item, lessonId);
            newItem.ifPresent(lessonItemTypeModel ->
                    this.lessonQuestionAnswerService.insertQuestions(item.getAnswers(), lessonItemTypeModel.getItemId()));

            return ResponseEntity.ok().build();
        }

        Optional<LessonItemTypeModel> newItem = lessonItemService.editQuestionItem(item);
        newItem.ifPresent(lessonItemTypeModel ->
                this.lessonQuestionAnswerService.insertQuestions(item.getAnswers(), lessonItemTypeModel.getItemId()));


        return ResponseEntity.ok().build();
    }

    @PostMapping(
            value = "/{lessonId}/text",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity editLessonContentTextItem(
            @PathVariable("lessonId") Integer lessonId,
            @RequestBody HttpCreateLessonTextItem item
    ) {

        if (item.getId() < 0) {
            Optional<LessonItemTypeModel> newItem = lessonItemService.addTextItem(item, lessonId);

            return ResponseEntity.ok().build();
        }

        Optional<LessonItemTypeModel> newItem = lessonItemService.editTextItem(item);

        return ResponseEntity.ok().build();
    }

    @DeleteMapping(
            value = "/{lessonId}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity deleteLesson(
            @PathVariable("lessonId") Integer lessonId
    ) {

        boolean deleted = lessonService.deleteLesson(lessonId);

        if (!deleted) {
            ResponseEntity
                    .badRequest()
                    .body("Cannot delete Lesson: " + lessonId);
        }

        return ResponseEntity
                .noContent()
                .build();
    }

    @PutMapping(
            value = "/{lessonId}",
            produces = MediaType.APPLICATION_JSON_VALUE

    )
    public ResponseEntity editLessonName(
            @PathVariable("lessonId") Integer lessonId,
            @RequestParam(name = "name") String name
    ) {

        Optional<LessonModel> optionalLessonModel = lessonService.editLessonName(lessonId, name);

        return optionalLessonModel.isEmpty()
                ? ResponseEntity.badRequest().body("Cannot edit lesson:" + lessonId)
                : ResponseEntity.ok().body(optionalLessonModel.get());
    }

    @PostMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity createLesson(
            @PathVariable("courseId") Integer courseId,
            @RequestParam(name = "name") String name
    ) throws URISyntaxException {
        Optional<LessonModel> lesson = lessonService.createLesson(courseId, name);

        return lesson.isPresent()
                ? ResponseEntity.created(new URI("/" + lesson.get().getLessonId())).build()
                : ResponseEntity.badRequest().body("Something gone wrong");
    }

    @DeleteMapping(
            value = "/item/{itemId}"
    )
    public ResponseEntity deleteItem(
            @PathVariable("itemId") Integer itemId
    ) {

        boolean deleted = lessonItemService.deleteItem(itemId);

        if (!deleted) {
            ResponseEntity
                    .badRequest()
                    .body("Cannot delete item: " + itemId);
        }

        return ResponseEntity
                .noContent()
                .build();
    }

    @PostMapping(
            value = "/{lessonId}/solve"
    )
    public ResponseEntity countPoints(
            Authentication authentication,
            @PathVariable("lessonId") Integer lessonId,
            @RequestBody List<HttpLessonPointsCountModel> content
    ) {
        String login = (String) authentication.getPrincipal();

        Optional<HttpLessonCompleationModel> optionalHttpLessonCompleationModel = lessonCompleationService.countPoints(login, lessonId, content);

        if (optionalHttpLessonCompleationModel.isEmpty()) {
            ResponseEntity
                    .ok()
                    .body(LessonState.UNFINISHED);
        }

        HttpLessonCompleationModel model = optionalHttpLessonCompleationModel.get();

        return ResponseEntity
                .ok()
                .body(model);
    }

    @GetMapping(
            value = "/solve"
    )
    public ResponseEntity getLessonsForCourse(
            Authentication authentication,
            @PathVariable("courseId") Integer courseId
    ) {
        String login = (String) authentication.getPrincipal();

        List<HttpCourseLessonModel> lessons = lessonCompleationService.getLessonsForCourses(login, courseId);

        if (lessons.isEmpty()) {
            ResponseEntity
                    .ok()
                    .body(new ArrayList<>());
        }

        return ResponseEntity
                .ok()
                .body(lessons);
    }
}
