package com.pl.uwb.mgr.r1.aism.moodlePro.database.subject.services;

import com.pl.uwb.mgr.r1.aism.moodlePro.core.services.PermissionsService;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.subject.model.SubjectModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.user.enums.UserRole;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.user.models.UserModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.user.service.UserService;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.subjects.HttpEditSubjectModel;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Getter
public class SubjectsService {
    private final PermissionsService permissionsService;
    private final UserService userService;

    private List<SubjectModel> subjects;

    @Autowired
    public SubjectsService(
            UserService userService,
            PermissionsService permissionsService
    ) {
        this.userService = userService;
        this.permissionsService = permissionsService;

        this.subjects = new ArrayList<>();

        this.subjects.add(new SubjectModel(1, "Informatyka", false));
        this.subjects.add(new SubjectModel(2, "Matematyka", false));
        this.subjects.add(new SubjectModel(3, "Fizyka", false));
    }

    public List<SubjectModel> getSubjects() {
        return new ArrayList<>(subjects);
    }

    public Optional<SubjectModel> findSubjectByName(String name) {
        return this.subjects.stream()
                .filter(subject -> subject.getName().equalsIgnoreCase(name))
                .findFirst();
    }

    public Optional<SubjectModel> findSubjectById(int id) {

        return subjects.stream()
                .filter(subjectModel -> subjectModel.getId() == id)
                .findFirst();
    }

    private int findFirstFreeId() {
        int size = this.subjects.size();
        int id = this.subjects.get(size - 1).getId() + 1;
        return id;
    }

    public List<SubjectModel> getAvailableSubjects(String login) {
        Optional<UserModel> userModelOpt = userService.findUserByLogin(login);

        if (userModelOpt.isEmpty()) {
            return new ArrayList<>();
        }

        UserModel userModel = userModelOpt.get();

        if (permissionsService.hasUserPermission(UserRole.ADMIN, userModel)) {
            return getCoursesForAdmin();
        }

        return getCoursesForUser();
    }

    private List<SubjectModel> getCoursesForUser() {
        return subjects.stream()
                .filter(subjectModel -> !subjectModel.isArchived())
                .collect(Collectors.toList());
    }

    private List<SubjectModel> getCoursesForAdmin() {
        return subjects;
    }

    public void deleteSubject(SubjectModel subjectModel) {
        subjectModel.setArchived(true);
    }

    public Optional<SubjectModel> createSubject(String subjectModel) {
        int id = findFirstFreeId();
        String name = subjectModel;

        SubjectModel newModel = new SubjectModel(id, name, false);

        this.subjects.add(newModel);

        return Optional.of(newModel);
    }

    public void unarchiveSubject(SubjectModel subject) {
        subject.setArchived(false);
    }

    public Optional<SubjectModel> editSubjectName(HttpEditSubjectModel httpEditSubjectModel) {
        int id = httpEditSubjectModel.getId();

        Optional<SubjectModel> optionalSubjectModel = findSubjectById(id);

        if (optionalSubjectModel.isPresent()) {
            String name = httpEditSubjectModel.getName();
            SubjectModel subjectModel = optionalSubjectModel.get();

            subjectModel.setName(name);

            return Optional.of(subjectModel);
        }

        return Optional.empty();
    }
}
