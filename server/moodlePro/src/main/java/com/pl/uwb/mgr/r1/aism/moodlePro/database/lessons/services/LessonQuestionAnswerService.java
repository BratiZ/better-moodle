package com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.services;

import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.models.LessonItemTypeModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.models.LessonQuestionAnswerModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.newModels.HttpCreateLessonAnswerItem;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LessonQuestionAnswerService {
    private List<LessonQuestionAnswerModel> answers;
    private Integer nextItemId = 25;

    @Autowired
    public LessonQuestionAnswerService() {
        this.answers = new ArrayList<>();

        initDataBase();
    }

    private void initDataBase() {
        this.answers.add(new LessonQuestionAnswerModel(1, 1, "a", true));
        this.answers.add(new LessonQuestionAnswerModel(2, 1, "b", false));
        this.answers.add(new LessonQuestionAnswerModel(5, 3, "a", true));
        this.answers.add(new LessonQuestionAnswerModel(6, 3, "b", false));
        this.answers.add(new LessonQuestionAnswerModel(7, 4, "a", true));
        this.answers.add(new LessonQuestionAnswerModel(8, 4, "b", false));
        this.answers.add(new LessonQuestionAnswerModel(3, 5, "a", true));
        this.answers.add(new LessonQuestionAnswerModel(4, 5, "b", false));
        this.answers.add(new LessonQuestionAnswerModel(9, 5, "c", true));
        this.answers.add(new LessonQuestionAnswerModel(10, 5, "d", false));
        this.answers.add(new LessonQuestionAnswerModel(11, 6, "a", false));
        this.answers.add(new LessonQuestionAnswerModel(12, 6, "b", true));

        this.answers.add(new LessonQuestionAnswerModel(13, 15, "a", true));
        this.answers.add(new LessonQuestionAnswerModel(14, 15, "b", false));
        this.answers.add(new LessonQuestionAnswerModel(15, 16, "a", true));
        this.answers.add(new LessonQuestionAnswerModel(16, 16, "b", false));
        this.answers.add(new LessonQuestionAnswerModel(17, 17, "a", true));
        this.answers.add(new LessonQuestionAnswerModel(18, 17, "b", false));
        this.answers.add(new LessonQuestionAnswerModel(19, 18, "a", true));
        this.answers.add(new LessonQuestionAnswerModel(20, 18, "b", false));
        this.answers.add(new LessonQuestionAnswerModel(21, 18, "c", true));
        this.answers.add(new LessonQuestionAnswerModel(22, 18, "d", false));
    }

    public List<LessonQuestionAnswerModel> getAnswersForQuestion(int questionId) {
        return this.answers.stream()
                .filter(answer -> answer.getQuestionId() == questionId)
                .collect(Collectors.toList());
    }

    public Optional<LessonQuestionAnswerModel> findAnswerById(int id) {
        return this.answers.stream()
                .filter(answer -> answer.getAnswerId() == id)
                .findFirst();
    }

    public void insertQuestions(HttpCreateLessonAnswerItem[] items, int questionId) {
        List<HttpCreateLessonAnswerItem> itemsToEdit = Arrays.stream(items).filter(item -> item.getId() >= 0).collect(Collectors.toList());
        List<HttpCreateLessonAnswerItem> itemsToAdd =  Arrays.stream(items).filter(item -> item.getId() < 0).collect(Collectors.toList());

        removeOldAnswers(itemsToEdit.stream().map(answers -> answers.getId()).collect(Collectors.toList()), questionId);
        addQuestions(itemsToAdd, questionId);
        editQuestions(itemsToEdit);
    }

    public void addQuestions(List<HttpCreateLessonAnswerItem> items, int questionId) {
        items.stream()
                .forEach(item -> this.answers.add(
                        LessonQuestionAnswerModel.of(item, nextItemId++, questionId)
                        )
                );
    }

    public void editQuestions(List<HttpCreateLessonAnswerItem> items) {
        items.forEach(
                item -> this.answers.stream()
                        .filter(savedItem -> savedItem.getAnswerId() == item.getId())
                        .findFirst()
                        .ifPresent(savedItem -> {
                            savedItem.setAnswer(item.getAnswer());
                            savedItem.setCorrect(item.getIsCorrect());
                        })
        );
    }

    public void removeOldAnswers(List<Integer> newAnswers, int questionId) {
        List<LessonQuestionAnswerModel> elementsToRemove = this.getAnswersForQuestion(questionId).stream()
                .filter(question -> !newAnswers.contains(question.getAnswerId()))
                .collect(Collectors.toList());

        removeAnswers(elementsToRemove);
    }

    public void removeAnswers(List<LessonQuestionAnswerModel> answersToRemove) {
        this.answers = this.answers.stream()
                .filter(answer -> !answersToRemove.contains(answer))
                .collect(Collectors.toList());
    }

    public List<LessonQuestionAnswerModel>findCorrectAnswers(int questionId) {
        return this.answers.stream()
                .filter(answer -> answer.getQuestionId() == questionId && answer.isCorrect())
                .collect(Collectors.toList());
    }
}
