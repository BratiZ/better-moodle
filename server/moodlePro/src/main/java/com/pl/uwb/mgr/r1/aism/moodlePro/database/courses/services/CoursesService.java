package com.pl.uwb.mgr.r1.aism.moodlePro.database.courses.services;

import com.pl.uwb.mgr.r1.aism.moodlePro.core.services.PermissionsService;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.courses.models.CourseModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.subject.model.SubjectModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.subject.services.SubjectsService;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.user.enums.UserRole;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.user.models.UserModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.user.service.UserService;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.courses.HttpEditCourseModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.courses.HttpNewCourseModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.utils.TimeService;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Getter
public class CoursesService {
    private final UserService userService;
    private final SubjectsService subjectService;
    private final TimeService timeService;
    private final PermissionsService permissionsService;
    private List<CourseModel> courses;

    @Autowired
    public CoursesService(
            UserService userService,
            SubjectsService subjectService,
            TimeService timeService,
            PermissionsService permissionsService
    ) {
        this.courses = new ArrayList<>();

        this.userService = userService;
        this.subjectService = subjectService;
        this.timeService = timeService;
        this.permissionsService = permissionsService;

        initDataBase();
    }

    private void initDataBase() {
        this.courses.add(new CourseModel(1, "Rekurencja", 1, timeService.newDate(2020, 5, 1),
                timeService.newDate(2020, 5, 30), 1, 10, 0, false));

        this.courses.add(new CourseModel(2, "Złożoność obliczeniowa", 1, timeService.newDate(2020, 5, 1),
                timeService.newDate(2020, 5, 30), 1, 10, 0, false));

        this.courses.add(new CourseModel(3, "Arytmetyka", 2, timeService.newDate(2020, 5, 1),
                timeService.newDate(2020, 5, 30), 1, 10, 0, true));


    }

    private int findFirstFreeId() {
        int size = this.courses.size();
        int id = this.courses.get(size - 1).getId() + 1;
        return id;
    }

    public Optional<CourseModel> findCourseById(int id) {
        return this.courses.stream()
                .filter(course -> course.getId() == id)
                .findFirst();
    }

    public List<CourseModel> getAvailableCourses(String login) {
        Optional<UserModel> userModelOpt = userService.findUserByLogin(login);

        if (userModelOpt.isEmpty()) {
            return new ArrayList<>();
        }

        UserModel userModel = userModelOpt.get();

        if (permissionsService.hasUserPermission(UserRole.ADMIN, userModel)) {
            return getCoursesForAdmin();
        }

        return getCoursesForUser();
    }

    private List<CourseModel> getCoursesForAdmin() {
        return courses;
    }

    private List<CourseModel> getCoursesForUser() {
        return courses.stream()
                .filter(courseModel -> !courseModel.isArchived())
                .collect(Collectors.toList());
    }

    public void deleteCourse(CourseModel courseModel) {
        courseModel.setArchived(true);
    }

    public void unarchiveCourse(CourseModel courseModel) {
        courseModel.setArchived(false);
    }

    public boolean stopCourse(Integer courseId) {

        Optional<CourseModel> courseModelOpt = this.findCourseById(courseId);

        if (courseModelOpt.isEmpty()) {
            return false;
        }

        courseModelOpt.get().setEnd(timeService.today());
        return true;
    }

    public Optional<CourseModel> createCourse(String login, HttpNewCourseModel courseModel) {
        int id = findFirstFreeId();
        String name = courseModel.getCourseName();
        int subjectId = courseModel.getSubjectId();
        Date start = timeService.parseDate(courseModel.getStartDate());
        Date end = timeService.parseDate(courseModel.getEndDate());

        Optional<UserModel> userModel = userService.findUserByLogin(login);
        Optional<SubjectModel> subjectModel = subjectService.findSubjectById(subjectId);

        if (userModel.isPresent() && subjectModel.isPresent()) {
            CourseModel newModel = new CourseModel(
                    id,
                    name,
                    subjectModel.get().getId(),
                    start,
                    end,
                    userModel.get().getId(),
                    0,
                    0,
                    false
            );

            this.courses.add(newModel);

            return Optional.of(newModel);
        }

        return Optional.empty();
    }

    public Optional<List<CourseModel>> getCoursesForSubject(Integer subjectId) {
        Optional<SubjectModel> optionalSubjectModel = subjectService.findSubjectById(subjectId);

        if (!optionalSubjectModel.isEmpty()) {
            List<CourseModel> coursesForSubject = new ArrayList<>();
            int id;
            for (CourseModel course : this.courses) {
                id = course.getSubjectId();
                if (id == subjectId) {
                    coursesForSubject.add(course);
                }
            }

            return Optional.of(coursesForSubject);
        }

        return Optional.empty();
    }

    public Optional<CourseModel> editCourseNameAndDate(Integer courseId, HttpEditCourseModel httpEditCourseNameAndDate) {
        Optional<CourseModel> optionalCourseModel = findCourseById(courseId);

        if (optionalCourseModel.isEmpty()) {
            return Optional.empty();
        }

        CourseModel course = optionalCourseModel.get();
        String name = httpEditCourseNameAndDate.getName();
        int subjectId = httpEditCourseNameAndDate.getSubjectId();
        Date start = timeService.parseDate(httpEditCourseNameAndDate.getStartDate());
        Date end = timeService.parseDate(httpEditCourseNameAndDate.getEndDate());

        course.setName(name);
        course.setSubjectId(subjectId);
        course.setStart(start);
        course.setEnd(end);

        return Optional.of(course);
    }
}
