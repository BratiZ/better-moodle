package com.pl.uwb.mgr.r1.aism.moodlePro.controllers.subjects;

import com.pl.uwb.mgr.r1.aism.moodlePro.database.courses.models.CourseModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.courses.services.CoursesService;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.subject.model.SubjectModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.subject.services.SubjectsService;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.subjects.HttpEditSubjectModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/subjects")
public class SubjectsController {
    private final SubjectsService subjectsService;
    private final CoursesService coursesService;

    @Autowired
    public SubjectsController(
            SubjectsService subjectsService,
            CoursesService coursesService
    ) {
        this.subjectsService = subjectsService;
        this.coursesService = coursesService;
    }

    @GetMapping()
    public ResponseEntity<List<SubjectModel>> getSubjectsList(Authentication authentication) {
        String login = (String) authentication.getPrincipal();
        List<SubjectModel> availableCourses = subjectsService.getAvailableSubjects(login);

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(availableCourses);
    }

    @GetMapping(
            value = "/{subjectsId}/info",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity getSubject(@PathVariable("subjectsId") Integer subjectId) {
        Optional<SubjectModel> subjectModelOpt = subjectsService.findSubjectById(subjectId);

        return subjectModelOpt.isPresent()
                ? ResponseEntity.ok().body(subjectModelOpt.get())
                : ResponseEntity.badRequest().body("Cannot find subject with ID: " + subjectId);
    }

    @PutMapping(
            value = "/{id}/delete"
    )
    public ResponseEntity deleteSubject(@PathVariable("id") String subjectId) {
        int id = Integer.parseInt(subjectId);
        Optional<SubjectModel> optionalSubjectModel = subjectsService.findSubjectById(id);

        if (optionalSubjectModel.isEmpty()) {
            return ResponseEntity
                    .badRequest()
                    .body("Subject do not exist");
        }

        SubjectModel subject = optionalSubjectModel.get();
        subjectsService.deleteSubject(subject);

        return ResponseEntity
                .ok()
                .build();
    }

    @PostMapping(
            value = "/{subjectName}"
    )
    public ResponseEntity createSubject(@PathVariable("subjectName") String subjectName) throws URISyntaxException {

        Optional<SubjectModel> newSubject = subjectsService.createSubject(subjectName);

        return newSubject.isPresent()
                ? ResponseEntity.created(new URI("/subjects/" + newSubject.get().getId())).build()
                : ResponseEntity.badRequest().body("Cannot create Subject");
    }

    @PutMapping(
            value = "/{id}/unarchive"
    )
    public ResponseEntity unarchiveSubject(@PathVariable("id") String subjectId) {
        int id = Integer.parseInt(subjectId);
        Optional<SubjectModel> optionalSubjectModel = subjectsService.findSubjectById(id);

        if (optionalSubjectModel.isEmpty()) {
            return ResponseEntity
                    .badRequest()
                    .body("Course with id \"" + subjectId + "\" do not exist");
        }

        SubjectModel subject = optionalSubjectModel.get();
        subjectsService.unarchiveSubject(subject);

        return ResponseEntity
                .ok()
                .build();
    }

    @GetMapping(
            value = "/{subjectId}/courses",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity getCoursesForSubject(@PathVariable("subjectId") Integer subjectId) {
        Optional<List<CourseModel>> optionalCourseModels = coursesService.getCoursesForSubject(subjectId);

        if (optionalCourseModels.isEmpty()) {
            return ResponseEntity
                    .badRequest()
                    .body("Subject doesn't exist");
        }

        List<CourseModel> courses = optionalCourseModels.get();

        return ResponseEntity
                .ok()
                .body(courses);
    }

    @PutMapping(
            value = "/editname"
    )
    public ResponseEntity editSubjectName(
            @RequestBody HttpEditSubjectModel httpEditSubjectModel
    ) {

        Optional<SubjectModel> optionalSubjectModel = subjectsService.editSubjectName(httpEditSubjectModel);

        if (optionalSubjectModel.isEmpty()) {
            ResponseEntity
                    .badRequest()
                    .body("Cannot edit");
        }

        SubjectModel subjectModel = optionalSubjectModel.get();
        return ResponseEntity
                .ok()
                .body(subjectModel);
    }

}
