package com.pl.uwb.mgr.r1.aism.moodlePro.database.questionnaires.service.resource.enums;

import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

@Getter
public enum ResourceName {
    PERSONALITY("Personality");
    String name;

    ResourceName(String name) {
        this.name = name;
    }

    public static Optional<ResourceName> of(String key) {
        return Arrays.stream(ResourceName.values())
                .filter(resourceName -> key.toUpperCase().equals(resourceName.getName().toUpperCase()))
                .findFirst();
    }

}
