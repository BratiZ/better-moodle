package com.pl.uwb.mgr.r1.aism.moodlePro.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    public void configure(WebSecurity web) {
        web.ignoring()
                .antMatchers("/login")
                .antMatchers("/register");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/*/questionnaire/**").authenticated()
                .antMatchers("/*/courses/**").authenticated()
                .antMatchers("/*/subjects/**").authenticated()
                .and()
                .addFilter(new JwtFilter(authenticationManager()))
                .csrf().disable();
    }
}
