package com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.edit;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HttpEditLessonNameModel {
    private int id;
    private String name;
}
