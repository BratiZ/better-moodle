package com.pl.uwb.mgr.r1.aism.moodlePro.database.user.enums;

import com.pl.uwb.mgr.r1.aism.moodlePro.controllers.questionnaries.enums.UserTypes;
import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

@Getter
public enum UserRole {
    ADMIN("ADMIN"),
    TEACHER("TEACHER"),
    USER("USER");

    private String key;

    UserRole(String userRoleKey) {
        this.key = userRoleKey;
    }

    public static Optional<UserRole> of(String role) {
        return Arrays.stream(UserRole.values())
                .filter(userRole -> userRole.getKey().equals(role))
                .findFirst();
    }
}
