package com.pl.uwb.mgr.r1.aism.moodlePro.controllers.questionnaries.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Data
@Getter
@AllArgsConstructor
public class UserAnswersModel {
    private Integer nr;
    private String type;
    private String[] answers;
}
