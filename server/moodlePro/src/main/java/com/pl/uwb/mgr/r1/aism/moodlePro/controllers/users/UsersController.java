package com.pl.uwb.mgr.r1.aism.moodlePro.controllers.users;

import com.pl.uwb.mgr.r1.aism.moodlePro.database.user.enums.UserRole;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.user.models.TeacherModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.user.models.UserModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.user.service.UserService;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UsersController {
    private final UserService userService;

    @Autowired
    public UsersController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(
            value = "/{userId}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity getUser(@PathVariable("userId") Integer userId) {
        Optional<UserModel> userModelOpt = userService.findUserById(userId);

        return userModelOpt.isPresent()
                ? ResponseEntity.ok().body(TeacherModel.of(userModelOpt.get()))
                : ResponseEntity.badRequest().body("Cannot find user with ID: " + userId);
    }

    @PutMapping(
        value = "/{userId}/{newRole}"
    )
    public ResponseEntity editUserRole(
            @PathVariable("userId") Integer userId,
            @PathVariable("newRole") String newRole
    ) {
        Optional<UserModel> userModelOpt = userService.editUserRole(userId, newRole);

        if(userModelOpt.isEmpty()) {
            return ResponseEntity
                    .badRequest()
                    .body("Cannot edit user role");
        }

        UserModel user = userModelOpt.get();

        return ResponseEntity
                .ok()
                .body(user);
    }

    @GetMapping(
            value = "/users",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity getAllUsers() {

        return ResponseEntity
                .ok()
                .body(userService.getUsers());
    }

    @GetMapping(
            value = "/{userId}/data",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity getUserData(@PathVariable("userId") Integer userId) {

        Optional<UserModel> optionalUserModel = userService.findUserById(userId);

        if (optionalUserModel.isEmpty()) {
            return ResponseEntity
                    .badRequest()
                    .body("User with id: " + userId + " doesnt exist");
        }

        UserModel userModel = optionalUserModel.get();

        return ResponseEntity
                .ok()
                .body(userModel);
    }

}
