package com.pl.uwb.mgr.r1.aism.moodlePro.http.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Data
@Getter
@AllArgsConstructor
public class HttpUserRegistrationModel {
    private String email;
    private String login;
    private String password;
}
