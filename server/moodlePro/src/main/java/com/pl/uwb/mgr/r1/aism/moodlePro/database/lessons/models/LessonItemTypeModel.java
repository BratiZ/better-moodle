package com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.enums.LessonContentType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract class LessonItemTypeModel {
    @JsonProperty("id")
    private int itemId;
    private int lessonId;
    private int position;
    private LessonContentType lessonContentType;
}
