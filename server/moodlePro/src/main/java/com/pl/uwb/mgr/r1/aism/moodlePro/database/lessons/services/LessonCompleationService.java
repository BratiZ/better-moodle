package com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.services;


import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.enums.LessonState;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.lessons.models.*;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.user.models.UserModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.database.user.service.UserService;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.newModels.HttpLessonCompleationModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.newModels.HttpLessonPointsCountModel;
import com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.oldModels.HttpCourseLessonModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LessonCompleationService {
    private List<LessonCompleationModel> states;

    private final LessonItemService lessonItemService;
    private final LessonQuestionAnswerService lessonQuestionAnswerService;
    private final UserService userService;
    private final LessonService lessonService;

    @Autowired
    public LessonCompleationService(
            LessonItemService lessonItemService,
            LessonQuestionAnswerService lessonQuestionAnswerService,
            UserService userService,
            LessonService lessonService
    ) {
        this.states = new ArrayList<>();

        this.lessonItemService = lessonItemService;
        this.lessonQuestionAnswerService = lessonQuestionAnswerService;
        this.userService = userService;
        this.lessonService = lessonService;

        initDataBase();
    }

    private void initDataBase() {

    }

    public Optional<HttpLessonCompleationModel> countPoints(String login, Integer lessonId, List<HttpLessonPointsCountModel> content) {
        int points = 0;
        Optional<LessonItemTypeModel> optionalQuestion;
        LessonQuestionItemModel question;
        List<Integer> ansewrsIds;
        Optional<LessonQuestionAnswerModel> optionalAnswer;
        List<LessonQuestionAnswerModel> questionCorrectAnswers;
        List<LessonQuestionAnswerModel> modelAnswers;
        for (HttpLessonPointsCountModel model : content) {
            optionalQuestion = lessonItemService.findItemById(model.getQuestionId());

            if (optionalQuestion.isPresent() && !model.getAnswers().isEmpty()) {
                question = (LessonQuestionItemModel) optionalQuestion.get();
                ansewrsIds = model.getAnswers();

                questionCorrectAnswers = lessonQuestionAnswerService.findCorrectAnswers(question.getItemId());

                if (ansewrsIds.size() == questionCorrectAnswers.size()) {
                    if (ansewrsIds.size() == 1) {
                        optionalAnswer = lessonQuestionAnswerService.findAnswerById(ansewrsIds.get(0));
                        if (optionalAnswer.isPresent()) {
                            points = optionalAnswer.get().isCorrect() ?
                                    points + 1
                                    : points;
                        }
                    } else {
                        modelAnswers = new ArrayList<>();
                        for (Integer answerId : ansewrsIds) {
                            optionalAnswer = lessonQuestionAnswerService.findAnswerById(answerId);
                            if (optionalAnswer.isPresent()) {
                                if (optionalAnswer.get().isCorrect()) {
                                    modelAnswers.add(optionalAnswer.get());
                                }
                            }
                        }
                        if (modelAnswers.size() == questionCorrectAnswers.size()) {
                            points++;
                        }
                    }
                }
            }


        }

        Optional<UserModel> userModel = userService.findUserByLogin(login);
        double percentage = points * 1.0 / content.size();
        LessonState lessonState;
        if (percentage > 0.5) {
            lessonState = LessonState.PASSED;
        } else {
            lessonState = LessonState.FAIL;
        }

        HttpLessonCompleationModel httpModel = new HttpLessonCompleationModel(lessonId, lessonState, points, content.size());

        Optional<LessonCompleationModel> optionalLessonCompleationModel = findStateByLessonId(httpModel.getLessonId());

        if (optionalLessonCompleationModel.isPresent()) {
            LessonCompleationModel lessonCompleationModel = optionalLessonCompleationModel.get();
            lessonCompleationModel.setLessonState(lessonState);
        } else {
            this.states.add(new LessonCompleationModel(userModel.get().getId(), lessonId, lessonState));
        }


        return Optional.of(httpModel);
    }

    private Optional<LessonCompleationModel> findStateByLessonId(int lessonId) {
        return this.states.stream()
                .filter(state -> state.getLessonId() == lessonId)
                .findFirst();
    }

    public List<HttpCourseLessonModel> getLessonsForCourses(String login, int courseId) {
        List<HttpCourseLessonModel> lessons = new ArrayList<>();
        Optional<UserModel> optionalUserModel = userService.findUserByLogin(login);
        List<LessonModel> courseLessons = lessonService.getLessonsModelsForCourse(courseId);

        for (LessonModel model : courseLessons) {
            Optional<LessonCompleationModel> optionalLessonCompleationModel = findLessonById(model.getLessonId());
            boolean bool = optionalLessonCompleationModel.isPresent();
            HttpCourseLessonModel httpModel;
            if (optionalLessonCompleationModel.isPresent()) {
                httpModel = HttpCourseLessonModel.of(optionalLessonCompleationModel.get(), model.getName());
            } else {
                LessonCompleationModel newModel = new LessonCompleationModel(
                        optionalUserModel.get().getId(),
                        model.getLessonId(),
                        LessonState.UNFINISHED
                );
                httpModel = HttpCourseLessonModel.of(newModel, model.getName());
                this.states.add(newModel);
            }
            lessons.add(httpModel);

        }

        return lessons;
    }

    private Optional<LessonCompleationModel> findLessonById(int lessonId) {
        return this.states.stream()
                .filter(state -> state.getLessonId() == lessonId)
                .findFirst();
    }
}
