package com.pl.uwb.mgr.r1.aism.moodlePro.http.lessons.newModels;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class HttpCreateLessonQuestionItem implements HttpCreateLessonItem {
    private Integer id;
    private Boolean multiAnswer;
    private String question;
    private HttpCreateLessonAnswerItem[] answers;

}
